﻿using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Responses.Fee;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Api.ApiMethodsInterfaces
{
    public interface IFeeApiMethods
    {
        Task<ApiResult<IEnumerable<FeeResponse>>> ListAsync(FilterRequest filterRequest = null);
    }
}
