﻿using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Requests.LoanPurpose;
using ProductManagement.Api.Responses.LoanPurpose;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Api.ApiMethodsInterfaces
{
    public interface ILoanPurposeApiMethods
    {
        Task<ApiResult<IEnumerable<LoanPurposeResponse>>> ListAsync(QueryRequest queryRequest = null);
        Task<ApiResult<LoanPurposeResponse>> GetAsync(string id);
        Task<ApiResult<LoanPurposeResponse>> CreateAsync(LoanPurposeCreateRequest request);
        Task<ApiResult<LoanPurposeResponse>> UpdateAsync(string id, LoanPurposeUpdateRequest request);
        Task<ApiResult<bool>> DeleteManyAsync(IEnumerable<string> ids);
    }
}
