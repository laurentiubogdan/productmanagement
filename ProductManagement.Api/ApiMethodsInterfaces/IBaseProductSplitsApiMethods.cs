﻿using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests.BaseProductSplits;
using ProductManagement.Api.Responses.BaseProductSplits;
using System.Threading.Tasks;

namespace ProductManagement.Api.ApiMethodsInterfaces
{
    public interface IBaseProductSplitsApiMethods
    {
        Task<ApiResult<BaseProductSplitsResponse>> GetAsync(string id);
        Task<ApiResult<BaseProductSplitsResponse>> CreateAsync(BaseProductSplitsCreateRequest request);
        Task<ApiResult<BaseProductSplitsResponse>> UpdateAsync(BaseProductSplitsUpdateRequest request);
    }
}
