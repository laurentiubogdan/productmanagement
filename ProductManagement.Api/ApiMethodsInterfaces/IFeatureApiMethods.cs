﻿using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Responses.Feature;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Api.ApiMethodsInterfaces
{
    public interface IFeatureApiMethods
    {
        Task<ApiResult<IEnumerable<FeatureResponse>>> ListAsync(QueryRequest queryRequest = null);
        Task<ApiResult<FeatureResponse>> GetAsync(string id);
        Task<ApiResult<bool>> DeleteManyAsync(IEnumerable<string> ids);
    }
}
