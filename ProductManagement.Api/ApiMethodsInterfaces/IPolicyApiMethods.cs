﻿using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Responses.Policy;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Api.ApiMethodsInterfaces
{
    public interface IPolicyApiMethods
    {
        Task<ApiResult<IEnumerable<PolicyResponse>>> ListAsync(FilterRequest filterRequest = null);
        Task<ApiResult<PolicyResponse>> GetAsync(string id);
        Task<ApiResult<bool>> DeleteManyAsync(IEnumerable<string> ids);
    }
}
