﻿using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Requests.BaseProduct;
using ProductManagement.Api.Responses.BaseProduct;
using ProductManagement.Api.Responses.Feature;
using ProductManagement.Api.Responses.LoanPurpose;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Api.ApiMethodsInterfaces
{
    public interface IBaseProductApiMethods
    {
        Task<ApiResult<IEnumerable<BaseProductResponse>>> ListAsync(QueryRequest queryRequest = null);
        Task<ApiResult<BaseProductResponse>> GetAsync(string id);
        Task<ApiResult<BaseProductResponse>> CreateAsync(BaseProductCreateRequest request);
        Task<ApiResult<BaseProductResponse>> UpdateAsync(string id, BaseProductUpdateRequest request);
        Task<ApiResult<bool>> DeleteManyAsync(IEnumerable<string> ids);
        Task<ApiResult<IEnumerable<LoanPurposeResponse>>> ListLoanPurposes(string baseProductId, QueryRequest queryRequest = null);
        Task<ApiResult<IEnumerable<ProductFeatureResponse>>> ListFeatures(string baseProductId, QueryRequest queryRequest = null);
    }
}
