﻿using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Requests.Rule;
using ProductManagement.Api.Responses.Rule;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Api.ApiMethodsInterfaces
{
    public interface IJavaScriptRuleApiMethods
    {
        Task<ApiResult<IEnumerable<JavaScriptRuleResponse>>> ListAsync();
        Task<ApiResult<IEnumerable<JavaScriptRuleResponse>>> ListQueryAsync(FilterRequest request);
        Task<ApiResult<JavaScriptRuleResponse>> CreateAsync(JavaScriptRuleCreateRequest request);
        Task<ApiResult<JavaScriptRuleResponse>> GetAsync(string id);
    }
}
