﻿using ProductManagement.Api.Requests.BaseProduct;
using ProductManagement.Api.Requests.Terms;
using ProductManagement.Domain;
using ProductManagement.Domain.Helper_Objects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProductManagement.Api.Helpers
{
    public static class ExtensionMethodsHelper
    {
        public static string GenerateUniqueCode(this string name)
        {
            string generatedCode = string.Empty;
            string[] seperatedWords = name.Split(' ');

            foreach (string word in seperatedWords)
            {
                generatedCode += "" + word[0] + word[word.Length - 1];
            }

            return generatedCode.ToUpper();
        }

        public static Enums.ProductTypeEnum? ToTypeEnum(this string type)
        {
            Enums.ProductTypeEnum? productType = null;

            if (type != null)
            {
                productType = (Enums.ProductTypeEnum?)Enum.Parse(typeof(Enums.ProductTypeEnum), type);
            }

            return productType;
        }

        public static Enums.ProductTypeEnum? ToTypeEnum(this string type, bool isNull)
        {
            if (type == null)
            {
                throw new ArgumentException("productType cannot be null");
            }

            Enums.ProductTypeEnum? productType = (Enums.ProductTypeEnum?)Enum.Parse(typeof(Enums.ProductTypeEnum), type);

            return productType;
        }

        public static Enums.InterestTypeEnum? ToInterestEnum(this string interest)
        {
            Enums.InterestTypeEnum? productInterest = null;

            if (interest == null || interest == string.Empty)
            {

                return null;
            }

            productInterest = (Enums.InterestTypeEnum?)Enum.Parse(typeof(Enums.InterestTypeEnum), interest);

            return productInterest;
        }

        public static Enums.StatusEnum? ToStatusEnum(this string status)
        {
            Enums.StatusEnum? productStatus = null;

            if (status != null)
            {
                productStatus = (Enums.StatusEnum?)Enum.Parse(typeof(Enums.StatusEnum), status);
            }

            return productStatus;
        }

        public static List<Enums.PaymentTypeEnum> ToListPaymentEnum(this IEnumerable<string> paymentTypes)
        {
            if (paymentTypes == null || paymentTypes.Count() <= 0 || paymentTypes.First() == null)
            {
                return null;
            }

            List<Enums.PaymentTypeEnum> paymentTypeEnums = new List<Enums.PaymentTypeEnum>();

            foreach (string paymentType in paymentTypes)
            {
                Enums.PaymentTypeEnum pt = (Enums.PaymentTypeEnum)Enum.Parse(typeof(Enums.PaymentTypeEnum), paymentType);
                paymentTypeEnums.Add(pt);
            }

            return paymentTypeEnums;
        }

        public static IEnumerable<Enums.PaymentTypeEnum> ToListPaymentEnum(this IEnumerable<Enums.PaymentTypeEnum> paymentTypes)
        {
            if (paymentTypes == null)
            {
                return Enumerable.Empty<Enums.PaymentTypeEnum>();
            }

            return paymentTypes;
        }

        public static IEnumerable<ProductFeatureLite> ToListFeatures(this IEnumerable<ProductFeatureLite> features)
        {
            if (features == null)
            {
                return Enumerable.Empty<ProductFeatureLite>();
            }

            return features;
        }

        public static IEnumerable<ProductPolicyLite> ToListPolicies(this IEnumerable<ProductPolicyLite> policies)
        {
            if (policies == null)
            {
                return Enumerable.Empty<ProductPolicyLite>();
            }

            return policies;
        }

        public static IEnumerable<JavascriptRule> ToListRules(this IEnumerable<JavascriptRule> rules)
        {
            if (rules == null)
            {
                return Enumerable.Empty<JavascriptRule>();
            }

            return rules;
        }

        public static string ToStringSeparatedWithComma(this IEnumerable<string> paymentTypes)
        {
            List<string> types = new List<string>();
            if (paymentTypes == null || paymentTypes.Count() <= 0)
            {
                types.Add("-");
            }
            else
            {
                types = paymentTypes.ToList();
            }

            return string.Join(",", types);
        }

        public static string ToStringWithSpaces(this string value)
        {
            string newString = string.Concat(value.Select(x => Char.IsUpper(x) ? " " + x : x.ToString())).TrimStart(' ');

            return newString;
        }

        public static string ToStringWithoutSpaces(this string value)
        {
            value = value.Replace(" ", "");
            return value;
        }

        public static IEnumerable<string> ToStringEnum(this string value)
        {
            IEnumerable<string> test = value.Split(',').ToList();

            return test;
        }

        public static IEnumerable<string> ToArrayIfNull(this IEnumerable<string> stringArray)
        {
            if (stringArray == null)
            {
                return Enumerable.Empty<string>();
            }

            return stringArray;
        }

        public static IEnumerable<Term> ToArrayIfNull(this IEnumerable<Term> termArray)
        {
            if (termArray == null)
            {
                return Enumerable.Empty<Term>();
            }

            return termArray;
        }

        public static IEnumerable<Interval> ToArrayIfNull(this IEnumerable<Interval> intervalArray)
        {
            if (intervalArray == null)
            {
                return Enumerable.Empty<Interval>();
            }

            return intervalArray;
        }

        public static IEnumerable<ProductFeatureLite> ToArrayIfNull(this IEnumerable<ProductFeatureLite> productFeatureLiteArray)
        {
            if (productFeatureLiteArray == null)
            {
                return Enumerable.Empty<ProductFeatureLite>();
            }

            return productFeatureLiteArray;
        }

        public static string ToDesignView(this string value)
        {
            if (value == null || value == "0")
            {
                return "-";
            }

            var mainString = "";
            char precedentChar = 'A';

            foreach (char x in value )
            {
                if (Char.IsUpper(x) && Char.IsLower(precedentChar))
                {
                    mainString = string.Format("{0} {1}",mainString, x);
                } else
                {
                    mainString = string.Format("{0}{1}", mainString, x);
                }
                precedentChar = x;
            }

            return mainString.TrimStart();

                //return string.Concat(value.Select(x => Char.IsUpper(x) && !Char.IsLower(precedentChar) ? " " + x : x.ToString())).TrimStart(' ');
        }

        public static string ToDesignView(this IEnumerable<string> value)
        {
            if (value.Count() == 0 || value == null)
            {
                return "-";
            }
            string newString = string.Join(", ", value);

            return string.Concat(newString.Select(x => Char.IsUpper(x) ? " " + x : x.ToString())).TrimStart(' ');
        }

        public static string ToTermDesignView(this IEnumerable<Term> terms)
        {
            if (terms.Count() == 0 || terms == null)
            {
                return "-";
            }

            string frequency = terms.FirstOrDefault().Frequency;

            List<int> intList = new List<int>();
            foreach (var term in terms)
            {
                intList.Add(term.Value);
            }
            intList.Sort();

            List<string> mainString = new List<string>();

            string firstString = "";
            string lineString = "";
            string normalString = "";
            int index = 0;

            int precendentNumber = 0;
            int lineLoops = 0;

            foreach (int number in intList)
            {
                index++;

                if (precendentNumber == 0)
                {
                    precendentNumber = number;
                    normalString = number.ToString();
                    continue;
                }

                if (number == precendentNumber + 1)
                {
                    if (lineLoops == 0)
                    {
                        firstString = precendentNumber.ToString();
                    }
                    lineLoops++;

                    lineString = String.Format("{0}-{1}", firstString, number);
                    precendentNumber = number;
                    normalString = "";
                    continue;
                }
                else
                {
                    lineLoops = 0;

                    if (lineString != "")
                    {
                        mainString.Add(lineString);
                        lineString = "";
                    }

                    if (normalString != "")
                    {
                        mainString.Add(normalString);
                        normalString = "";
                    }

                    normalString = String.Format("{0}", number);
                    precendentNumber = number;
                    continue;
                }
            }

            if(index == 1)
            {
                return String.Format("{0} {1}",precendentNumber.ToString(), frequency);
            } else
            {
                if (lineString != "")
                {
                    mainString.Add(lineString);
                }

                if (normalString != "")
                {
                    mainString.Add(normalString);
                }

                return String.Format("{0} {1}", string.Join(", ", mainString), frequency);
            }
        }

        public static string ToLoanAmountDesignView(this IEnumerable<Interval> intervals, bool isForInput = false)
        {
            if (intervals.Count() == 0 || intervals == null)
            {
                return (isForInput ? "" : "-");
            }

            List<string> stringList = new List<string>();

            foreach (var interval in intervals)
            {
                string newString = ($"${interval.Min.ToString("N")} - ${interval.Max.ToString("N")}");
                stringList.Add(newString);
            }

            return string.Join(", ", stringList);
        }

        //public static string ToLoanAmountDesignView(this int interval)
        //{
        //    List<string> stringList = new List<string>();

        //    return String.Format("${0}", interval.ToString("N"));
        //}

        public static string ToLvrDesignView(this IEnumerable<Interval> intervals, bool isForInput = false)
        {
            if (intervals.Count() == 0 || intervals == null)
            {
                return (isForInput ? "" : "-");
            }

            List<string> stringList = new List<string>();

            foreach (var interval in intervals)
            {
                string newString = ($"{interval.Min}% - {interval.Max}%");
                stringList.Add(newString);
            }

            return string.Join(", ", stringList);
        }

        public static string ToTermDesignView(this TermUpdateRequest request)
        {
            if (request.Frequency == null)
            {
                return "";
            }

            string frequency = request.Frequency.ToString();

            List<int> intList = request.Values.ToList();

            intList.Sort();

            List<string> mainString = new List<string>();

            string firstString = "";
            string lineString = "";
            string normalString = "";
            int index = 0;

            int precendentNumber = 0;
            int lineLoops = 0;

            foreach (int number in intList)
            {
                index++;

                if (precendentNumber == 0)
                {
                    precendentNumber = number;
                    normalString = number.ToString();
                    continue;
                }

                if (number == precendentNumber + 1)
                {
                    if (lineLoops == 0)
                    {
                        firstString = precendentNumber.ToString();
                    }
                    lineLoops++;

                    lineString = String.Format("{0}-{1}", firstString, number);
                    precendentNumber = number;
                    normalString = "";
                    continue;
                }
                else
                {
                    lineLoops = 0;

                    if (lineString != "")
                    {
                        mainString.Add(lineString);
                        lineString = "";
                    }

                    if (normalString != "")
                    {
                        mainString.Add(normalString);
                        normalString = "";
                    }

                    normalString = String.Format("{0}", number);
                    precendentNumber = number;
                    continue;
                }
            }

            if (index == 1)
            {
                return precendentNumber.ToString();
            }
            else
            {
                if (lineString != "")
                {
                    mainString.Add(lineString);
                }

                if (normalString != "")
                {
                    mainString.Add(normalString);
                }

                return string.Join(",", mainString);
            }
        }

        public static string ToBaseInterestRateDesignView(this decimal baseInterest)
        {
            return String.Format("{0}%", baseInterest);
        }
    }
}