﻿using System;

namespace ProductManagement.Api.Helpers
{
    public class ApiResult<T>
    {
        public ApiResult()
        { }

        public Exception Exception { get; set; }
        public bool HasException
        {
            get
            {
                return Exception != null;
            }
        }

        public T Result { get; set; }
    }
}
