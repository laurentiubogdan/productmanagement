﻿
namespace ProductManagement.Api.Responses.LoanPurpose
{
    public class LoanPurposeResponse
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
