﻿using ProductManagement.Api.Responses.Rule;
using System.Collections.Generic;

namespace ProductManagement.Api.Responses.Policy
{
    public class ProductPolicyResponse
    {
        public PolicyResponse Policy { get; set; }
        public IEnumerable<JavaScriptRuleResponse> Rules { get; set; }
        public string Status { get; set; }
    }
}
