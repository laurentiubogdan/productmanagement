﻿namespace ProductManagement.Api.Responses.Rule
{
    public class JavaScriptRuleResponse
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Rule { get; set; }
        public string RuleType { get; set; }
    }
}
