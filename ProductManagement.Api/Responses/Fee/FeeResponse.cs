﻿using ProductManagement.Domain;
using ProductManagement.Domain.Helper_Objects;
using System.Collections.Generic;

namespace ProductManagement.Api.Responses.Fee
{
    public class FeeResponse
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public IEnumerable<JavascriptRule> Rules { get; set; }
        public IEnumerable<AmountObject> Amounts { get; set; }
    }
}
