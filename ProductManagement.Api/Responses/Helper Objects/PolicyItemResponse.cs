﻿using ProductManagement.Api.Responses.Rule;
using System.Collections.Generic;

namespace ProductManagement.Api.Responses.Helper_Objects
{
    public class PolicyItemResponse
    {
        public string Name { get; set; }
        public string Status { get; set; }
        public IEnumerable<JavaScriptRuleResponse> ItemRules { get; set; }
    }
}
