﻿using ProductManagement.Api.Responses.Rule;
using System.Collections.Generic;

namespace ProductManagement.Api.Responses.Feature
{
    public class FeatureResponse
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public IEnumerable<JavaScriptRuleResponse> Rules { get; set; }
    }
}
