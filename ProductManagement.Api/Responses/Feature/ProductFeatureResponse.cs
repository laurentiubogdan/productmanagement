﻿using ProductManagement.Api.Responses.Rule;
using System.Collections.Generic;

namespace ProductManagement.Api.Responses.Feature
{
    public class ProductFeatureResponse
    {
        public FeatureResponse Feature { get; set; }
        public IEnumerable<JavaScriptRuleResponse> Rules { get; set; }
        public string Status { get; set; }
    }
}
