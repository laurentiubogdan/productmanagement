﻿using ProductManagement.Domain;
using System.Collections.Generic;

namespace ProductManagement.Api.Responses.Feature
{
    public class FeatureUpdateResponse
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public IEnumerable<JavascriptRule> Rules { get; set; }
    }
}
