﻿using ProductManagement.Domain;
using ProductManagement.Domain.Helper_Objects;
using System.Collections.Generic;

namespace ProductManagement.Api.Responses.BaseProductSplits
{
    public class BaseProductSplitsResponse
    {
        public string Id { get; set; }
        public IEnumerable<Split> Splits { get; set; }
        public IEnumerable<Split> FilteredSplits { get; set; }
        public IEnumerable<JavaScriptRuleReference> Rules { get; set; }
    }
}
