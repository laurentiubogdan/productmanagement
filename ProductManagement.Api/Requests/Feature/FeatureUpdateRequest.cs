﻿using ProductManagement.Domain;
using ProductManagement.Domain.Helper_Objects;
using System.Collections.Generic;

namespace ProductManagement.Api.Requests.Feature
{
    public class FeatureUpdateRequest
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Enums.FeatureTypeEnum Type { get; set; }
        public Enums.StatusEnum Status { get; set; }
        public IEnumerable<JavascriptRule> Rules { get; set; }
    }
}
