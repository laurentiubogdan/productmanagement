﻿using ProductManagement.Domain.Helper_Objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductManagement.Api.Requests.BaseProduct
{
    public class BaseProductCreateRequest
    {
        public string Name { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public Enums.FunderEnum Funder { get; set; }
        public Enums.ProductTypeEnum Type { get; set; }
    }
}
