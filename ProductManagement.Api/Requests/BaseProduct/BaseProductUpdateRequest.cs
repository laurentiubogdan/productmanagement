﻿using ProductManagement.Api.Requests.Terms;
using ProductManagement.Domain.Helper_Objects;
using System.Collections.Generic;

namespace ProductManagement.Api.Requests.BaseProduct
{
    public class BaseProductUpdateRequest
    {
        public string Name { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public Enums.FunderEnum Funder { get; set; } // valorile vor veni dintr-un dropdown - enum
        public Enums.ProductTypeEnum Type { get; set; } // valorile for veni dintr-un dropdown - enum
        public IEnumerable<Enums.DocumentationType> DocumentationType { get; set; }
        public IEnumerable<Enums.PrimaryLoanPurpose> PrimaryLoanPurpose { get; set; }
        public TermUpdateRequest PrincipalInterestTerm { get; set; }
        public TermUpdateRequest InterestOnlyTerm { get; set; }
        public TermUpdateRequest PrepaidInterestTerm { get; set; }
        public TermUpdateRequest InterestCapitalisedTerm { get; set; }
        public TermUpdateRequest PrincipalInterestFeesTerm { get; set; }
        public TermUpdateRequest VariableRateTerm { get; set; }
        public TermUpdateRequest FixedRateTerm { get; set; }
        public IEnumerable<Interval> LoanAmount { get; set; }
        public IEnumerable<Interval> Lvr { get; set; }
        public IEnumerable<Enums.RepaymentMethodsEnum> RepaymentMethods { get; set; }
        public IEnumerable<Enums.RepaymentFrequencyEnum> RepaymentFrequency { get; set; }
        public IEnumerable<Enums.StatementCycleEnum> StatementCycle { get; set; }
        public IEnumerable<Enums.SecurityPriorityEnum> SecurityPriority { get; set; }
        public int NumberOfSplits { get; set; }
        public int GenuineSavings { get; set; }
        public decimal BaseInterestRate { get; set; }
        public IEnumerable<string> LoanPurposes { get; set; }
        public IEnumerable<ProductFeatureLite> Features { get; set; }
    }
}
