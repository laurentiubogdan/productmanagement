﻿using ProductManagement.Domain.Helper_Objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductManagement.Api.Requests.Terms
{
    public class TermUpdateRequest
    {
        public IEnumerable<int> Values { get; set; }
        public Enums.TermFrequencyEnum? Frequency { get; set; }
    }
}
