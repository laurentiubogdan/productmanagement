﻿using System.Collections.Generic;

namespace ProductManagement.Api.Requests
{
    public class ProjectionRequest
    {
        public IEnumerable<string> ProjectionFields { get; set; }
    }
}
