﻿
namespace ProductManagement.Api.Requests.LoanPurpose
{
    public class LoanPurposeCreateRequest
    {
        public string Name { get; set; }
    }
}
