﻿
namespace ProductManagement.Api.Requests.LoanPurpose
{
    public class LoanPurposeUpdateRequest
    {
        public string Name { get; set; }
    }
}
