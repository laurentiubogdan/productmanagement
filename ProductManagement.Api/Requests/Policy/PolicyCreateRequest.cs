﻿using ProductManagement.Domain;
using ProductManagement.Domain.Helper_Objects;
using System.Collections.Generic;

namespace ProductManagement.Api.Requests.Policy
{
    public class PolicyCreateRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public Enums.PolicyTypeEnum Type { get; set; }
        public Enums.StatusEnum Status { get; set; }
        public IEnumerable<PolicyItem> Items { get; set; }
        public IEnumerable<JavascriptRule> Rules { get; set; }
    }
}
