﻿
using System.Collections.Generic;

namespace ProductManagement.Api.Requests
{
    public class QueryRequest
    {
        public ProjectionRequest ProjectionRequest { get; set; }
        public IEnumerable<FilterRequest> FilterRequests { get; set; }
        public IEnumerable<SortRequest> SortRequests { get; set; }
        public int Limit { get; set; }
        public int Skip { get; set; }
    }
}
