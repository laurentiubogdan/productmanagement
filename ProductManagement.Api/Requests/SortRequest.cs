﻿
namespace ProductManagement.Api.Requests
{
    public class SortRequest
    {
        public string Field { get; set; }
        public string Direction { get; set; }
    }
}
