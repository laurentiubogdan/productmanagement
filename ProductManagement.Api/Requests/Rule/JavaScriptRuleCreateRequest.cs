﻿namespace ProductManagement.Api.Requests.Rule
{
    public class JavaScriptRuleCreateRequest
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Rule { get; set; }
        public string RuleType { get; set; }
    }
}
