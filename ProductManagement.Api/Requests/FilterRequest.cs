﻿using System.Collections.Generic;

namespace ProductManagement.Api.Requests
{
    public class FilterRequest
    {
        public string Property { get; set; }
        public string Operator { get; set; }
        public IEnumerable<string> Values { get; set; }
    }
}
