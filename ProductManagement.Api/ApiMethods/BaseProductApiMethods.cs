﻿using Newtonsoft.Json;
using ProductManagement.Api.ApiMethodsInterfaces;
using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Requests.BaseProduct;
using ProductManagement.Api.Responses.BaseProduct;
using ProductManagement.Api.Responses.Feature;
using ProductManagement.Api.Responses.LoanPurpose;
using ProductManagement.Domain.Helper_Objects;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProductManagement.Api.ApiMethods
{
    public class BaseProductApiMethods : IBaseProductApiMethods
    {

        private BackendUrlApi _apiSettings;
        private readonly IHttpClientFactory _clientFactory;

        public BaseProductApiMethods(BackendUrlApi apiSettings, IHttpClientFactory clientFactory)
        {
            _apiSettings = apiSettings;
            _clientFactory = clientFactory;
        }

        private string Url
        {
            get
            {
                return _apiSettings.BackendUrl;
            }
        }

        public async Task<ApiResult<IEnumerable<BaseProductResponse>>> ListAsync(QueryRequest queryRequest = null)
        {
            ApiResult<IEnumerable<BaseProductResponse>> apiResponse = new ApiResult<IEnumerable<BaseProductResponse>>();

            HttpClient client = _clientFactory.CreateClient();
            HttpResponseMessage httpResponse = new HttpResponseMessage();

            if (queryRequest == null)
            {
                string url = $"{Url}/base";

                httpResponse = await client.GetAsync(url);
            }
            else
            {
                string json = JsonConvert.SerializeObject(queryRequest);

                StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

                HttpRequestMessage request = new HttpRequestMessage
                {
                    Content = content,
                    Method = HttpMethod.Post,
                    RequestUri = new Uri($"{Url}/base/query")
                };

                httpResponse = await client.SendAsync(request);
            }

            if (httpResponse.IsSuccessStatusCode)
            {
                apiResponse.Result = await httpResponse.Content.ReadAsAsync<IEnumerable<BaseProductResponse>>();
            }
            else
            {
                apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
            }

            return apiResponse;
        }

        public async Task<ApiResult<BaseProductResponse>> GetAsync(string id)
        {
            ApiResult<BaseProductResponse> apiResponse = new ApiResult<BaseProductResponse>();
            HttpClient client = _clientFactory.CreateClient();

            string url = $"{Url}/base/{id}";

            HttpResponseMessage httpResponse = await client.GetAsync(url);

            if (httpResponse.IsSuccessStatusCode)
            {
                apiResponse.Result = await httpResponse.Content.ReadAsAsync<BaseProductResponse>();
            }
            else
            {
                apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
            }

            return apiResponse;
        }

        public async Task<ApiResult<BaseProductResponse>> CreateAsync(BaseProductCreateRequest request)
        {
            ApiResult<BaseProductResponse> apiResponse = new ApiResult<BaseProductResponse>();
            HttpClient client = _clientFactory.CreateClient();

            string url = $"{Url}/base";
            HttpResponseMessage httpResponse = await client.PostAsJsonAsync(url, request);

            if (httpResponse.IsSuccessStatusCode == false)
            {
                apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
            }
            else
            {
                apiResponse.Result = await httpResponse.Content.ReadAsAsync<BaseProductResponse>();
            }

            return apiResponse;
        }

        public async Task<ApiResult<BaseProductResponse>> UpdateAsync(string id, BaseProductUpdateRequest request)
        {
            ApiResult<BaseProductResponse> apiResponse = new ApiResult<BaseProductResponse>();
            HttpClient client = _clientFactory.CreateClient();

            string url = $"{Url}/base/{id}";

            HttpResponseMessage httpResponse = await client.PutAsJsonAsync(url, request);

            if (httpResponse.IsSuccessStatusCode)
            {
                apiResponse.Result = await httpResponse.Content.ReadAsAsync<BaseProductResponse>();
            }
            else
            {
                apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
            }

            return apiResponse;
        }

        public async Task<ApiResult<bool>> DeleteManyAsync(IEnumerable<string> ids)
        {
            ApiResult<bool> result = new ApiResult<bool>();
            HttpClient client = _clientFactory.CreateClient();

            string json = JsonConvert.SerializeObject(ids);

            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            HttpRequestMessage request = new HttpRequestMessage
            {
                Content = content,
                Method = HttpMethod.Delete,
                RequestUri = new Uri($"{Url}/base")
            };

            HttpResponseMessage apiResult = await client.SendAsync(request);

            if (apiResult.IsSuccessStatusCode == false)
            {
                result.Exception = new Exception(apiResult.ReasonPhrase);
            }

            return result;
        }

        public async Task<ApiResult<IEnumerable<LoanPurposeResponse>>> ListLoanPurposes(string baseProductId, QueryRequest queryRequest = null)
        {
            ApiResult<IEnumerable<LoanPurposeResponse>> apiResponse = new ApiResult<IEnumerable<LoanPurposeResponse>>();
            HttpClient client = _clientFactory.CreateClient();
            HttpResponseMessage httpResponse = new HttpResponseMessage();

            if (queryRequest == null)
            {
                string url = $"{Url}/base/{baseProductId}/loanPurposes";

                httpResponse = await client.GetAsync(url);
            }
            else
            {
                string url = $"{Url}/base/{baseProductId}/loanPurposes/query";

                httpResponse = await client.PostAsJsonAsync(url, queryRequest);
            }

            if (httpResponse.IsSuccessStatusCode)
            {
                apiResponse.Result = await httpResponse.Content.ReadAsAsync<IEnumerable<LoanPurposeResponse>>();
            }
            else
            {
                apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
            }

            return apiResponse;
        }

        public async Task<ApiResult<IEnumerable<ProductFeatureResponse>>> ListFeatures(string baseProductId, QueryRequest queryRequest = null)
        {
            ApiResult<IEnumerable<ProductFeatureResponse>> apiResponse = new ApiResult<IEnumerable<ProductFeatureResponse>>();
            HttpClient client = _clientFactory.CreateClient();
            HttpResponseMessage httpResponse = new HttpResponseMessage();

            if (queryRequest == null)
            {
                string url = $"{Url}/base/{baseProductId}/features";

                httpResponse = await client.GetAsync(url);
            }
            else
            {
                string url = $"{Url}/base/{baseProductId}/features/query";

                httpResponse = await client.PostAsJsonAsync(url, queryRequest);
            }

            if (httpResponse.IsSuccessStatusCode)
            {
                apiResponse.Result = await httpResponse.Content.ReadAsAsync<IEnumerable<ProductFeatureResponse>>();
            }
            else
            {
                apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
            }

            return apiResponse;
        }
    }
}
