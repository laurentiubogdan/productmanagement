﻿using ProductManagement.Api.ApiMethodsInterfaces;
using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Requests.Rule;
using ProductManagement.Api.Responses.Rule;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProductManagement.Api.ApiMethods
{
    public class JavaScriptRuleApiMethods : IJavaScriptRuleApiMethods
    {
        private BackendUrlApi _apiSettings;
        private IHttpClientFactory _clientFactory;

        public JavaScriptRuleApiMethods(BackendUrlApi apiSettings, IHttpClientFactory clientFactory)
        {
            _apiSettings = apiSettings;
            _clientFactory = clientFactory;
        }

        private string Url
        {
            get
            {
                return _apiSettings.BackendUrl;
            }
        }

        public async Task<ApiResult<IEnumerable<JavaScriptRuleResponse>>> ListAsync()
        {
            ApiResult<IEnumerable<JavaScriptRuleResponse>> apiResponse = new ApiResult<IEnumerable<JavaScriptRuleResponse>>();

            HttpClient client = _clientFactory.CreateClient();
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            string url = $"{Url}/rule";
            httpResponse = await client.GetAsync(url);

            if (httpResponse.IsSuccessStatusCode)
            {
                apiResponse.Result = await httpResponse.Content.ReadAsAsync<IEnumerable<JavaScriptRuleResponse>>();
            }
            else
            {
                apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
            }

            return apiResponse;
        }

        public async Task<ApiResult<IEnumerable<JavaScriptRuleResponse>>> ListQueryAsync(FilterRequest request)
        {
            ApiResult<IEnumerable<JavaScriptRuleResponse>> apiResponse = new ApiResult<IEnumerable<JavaScriptRuleResponse>>();
            HttpClient client = _clientFactory.CreateClient();
            string url = $"{Url}/rule/query";
            HttpResponseMessage httpResponse = await client.PostAsJsonAsync(url, request);

            if (httpResponse.IsSuccessStatusCode)
            {
                apiResponse.Result = await httpResponse.Content.ReadAsAsync<IEnumerable<JavaScriptRuleResponse>>();
            }
            else
            {
                apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
            }

            return apiResponse;
        }

        public async Task<ApiResult<JavaScriptRuleResponse>> CreateAsync(JavaScriptRuleCreateRequest request)
        {
            ApiResult<JavaScriptRuleResponse> apiResponse = new ApiResult<JavaScriptRuleResponse>();
            HttpClient client = _clientFactory.CreateClient();

            string url = $"{Url}/rule";
            HttpResponseMessage httpResponse = await client.PostAsJsonAsync(url, request);

            if (httpResponse.IsSuccessStatusCode)
            {
                apiResponse.Result = await httpResponse.Content.ReadAsAsync<JavaScriptRuleResponse>();
            }
            else
            {
                apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
            }

            return apiResponse;
        }

        public async Task<ApiResult<JavaScriptRuleResponse>> GetAsync(string id)
        {
            ApiResult<JavaScriptRuleResponse> apiResponse = new ApiResult<JavaScriptRuleResponse>();
            HttpClient client = _clientFactory.CreateClient();

            string url = $"{Url}/rule/{id}";
            HttpResponseMessage httpResponse = await client.GetAsync(url);
            if (httpResponse.IsSuccessStatusCode)
            {
                apiResponse.Result = await httpResponse.Content.ReadAsAsync<JavaScriptRuleResponse>();
            }
            else
            {
                apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
            }

            return apiResponse;
        }
    }
}
