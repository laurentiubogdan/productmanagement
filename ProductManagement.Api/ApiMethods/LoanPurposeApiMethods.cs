﻿using Newtonsoft.Json;
using ProductManagement.Api.ApiMethodsInterfaces;
using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Requests.LoanPurpose;
using ProductManagement.Api.Responses.LoanPurpose;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProductManagement.Api.ApiMethods
{
    public class LoanPurposeApiMethods : ILoanPurposeApiMethods
    {

        private BackendUrlApi _apiSettings;
        private readonly IHttpClientFactory _clientFactory;

        public LoanPurposeApiMethods(BackendUrlApi apiSettings, IHttpClientFactory clientFactory)
        {
            _apiSettings = apiSettings;
            _clientFactory = clientFactory;
        }

        private string Url
        {
            get
            {
                return _apiSettings.BackendUrl;
            }
        }

        public async Task<ApiResult<IEnumerable<LoanPurposeResponse>>> ListAsync(QueryRequest queryRequest = null)
        {
            ApiResult<IEnumerable<LoanPurposeResponse>> apiResponse = new ApiResult<IEnumerable<LoanPurposeResponse>>();

            HttpClient client = _clientFactory.CreateClient();
            HttpResponseMessage httpResponse = new HttpResponseMessage();

            if (queryRequest == null)
            {
                string url = $"{Url}/loanPurposes";

                httpResponse = await client.GetAsync(url);
            }
            else
            {
                string json = JsonConvert.SerializeObject(queryRequest);

                StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

                HttpRequestMessage request = new HttpRequestMessage
                {
                    Content = content,
                    Method = HttpMethod.Get,
                    RequestUri = new Uri($"{Url}/loanPurposes/query")
                };

                httpResponse = await client.SendAsync(request);
            }

            if (httpResponse.IsSuccessStatusCode)
            {
                apiResponse.Result = await httpResponse.Content.ReadAsAsync<IEnumerable<LoanPurposeResponse>>();
            }
            else
            {
                apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
            }

            return apiResponse;
        }

        public async Task<ApiResult<LoanPurposeResponse>> GetAsync(string id)
        {
            ApiResult<LoanPurposeResponse> apiResponse = new ApiResult<LoanPurposeResponse>();
            HttpClient client = _clientFactory.CreateClient();

            string url = $"{Url}/loanPurposes/{id}";

            HttpResponseMessage httpResponse = await client.GetAsync(url);

            if (httpResponse.IsSuccessStatusCode)
            {
                apiResponse.Result = await httpResponse.Content.ReadAsAsync<LoanPurposeResponse>();
            }
            else
            {
                apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
            }

            return apiResponse;
        }

        public async Task<ApiResult<LoanPurposeResponse>> CreateAsync(LoanPurposeCreateRequest request)
        {
            ApiResult<LoanPurposeResponse> apiResponse = new ApiResult<LoanPurposeResponse>();
            HttpClient client = _clientFactory.CreateClient();

            string url = $"{Url}/loanPurposes";
            HttpResponseMessage httpResponse = await client.PostAsJsonAsync(url, request);

            if (httpResponse.IsSuccessStatusCode == false)
            {
                apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
            }
            else
            {
                apiResponse.Result = await httpResponse.Content.ReadAsAsync<LoanPurposeResponse>();
            }

            return apiResponse;
        }

        public async Task<ApiResult<LoanPurposeResponse>> UpdateAsync(string id, LoanPurposeUpdateRequest request)
        {
            ApiResult<LoanPurposeResponse> apiResponse = new ApiResult<LoanPurposeResponse>();
            HttpClient client = _clientFactory.CreateClient();

            string url = $"{Url}/loanPurposes/{id}";

            HttpResponseMessage httpResponse = await client.PutAsJsonAsync(url, request);

            if (httpResponse.IsSuccessStatusCode)
            {
                apiResponse.Result = await httpResponse.Content.ReadAsAsync<LoanPurposeResponse>();
            }
            else
            {
                apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
            }

            return apiResponse;
        }

        public async Task<ApiResult<bool>> DeleteManyAsync(IEnumerable<string> ids)
        {
            ApiResult<bool> result = new ApiResult<bool>();
            HttpClient client = _clientFactory.CreateClient();

            string json = JsonConvert.SerializeObject(ids);

            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            HttpRequestMessage request = new HttpRequestMessage
            {
                Content = content,
                Method = HttpMethod.Delete,
                RequestUri = new Uri($"{Url}/loanPurposes")
            };

            HttpResponseMessage apiResult = await client.SendAsync(request);

            if (apiResult.IsSuccessStatusCode == false)
            {
                result.Exception = new Exception(apiResult.ReasonPhrase);
            }

            return result;
        }
    }
}
