﻿using Newtonsoft.Json;
using ProductManagement.Api.ApiMethodsInterfaces;
using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Responses.Policy;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProductManagement.Api.ApiMethods
{
    public class PolicyApiMethods : IPolicyApiMethods
    {

        private BackendUrlApi _apiSettings;

        public PolicyApiMethods(BackendUrlApi apiSettings)
        {
            _apiSettings = apiSettings;
        }

        private string Url
        {
            get
            {
                return _apiSettings.BackendUrl;
            }
        }
        public async Task<ApiResult<IEnumerable<PolicyResponse>>> ListAsync(FilterRequest filterRequest)
        {
            ApiResult<IEnumerable<PolicyResponse>> apiResponse = new ApiResult<IEnumerable<PolicyResponse>>();

            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage httpResponse = new HttpResponseMessage();

                if (filterRequest == null)
                {
                    string url = $"{Url}/policies";

                    httpResponse = await client.GetAsync(url);
                } else
                {
                    string json = JsonConvert.SerializeObject(filterRequest);

                    StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

                    HttpRequestMessage request = new HttpRequestMessage
                    {
                        Content = content,
                        Method = HttpMethod.Get,
                        RequestUri = new Uri($"{Url}/policies/query")
                    };

                    httpResponse = await client.SendAsync(request);
                }

                if (httpResponse.IsSuccessStatusCode)
                {
                    apiResponse.Result = await httpResponse.Content.ReadAsAsync<IEnumerable<PolicyResponse>>();
                }
                else
                {
                    apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
                }
            }
            return apiResponse;
        }

        public async Task<ApiResult<PolicyResponse>> GetAsync(string id)
        {
            ApiResult<PolicyResponse> apiResponse = new ApiResult<PolicyResponse>();

            using (HttpClient client = new HttpClient())
            {
                string url = $"{Url}/policies/{id}";

                HttpResponseMessage httpResponse = await client.GetAsync(url);

                if (httpResponse.IsSuccessStatusCode)
                {
                    apiResponse.Result = await httpResponse.Content.ReadAsAsync<PolicyResponse>();
                }
                else
                {
                    apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
                }

                return apiResponse;
            }
        }

        public async Task<ApiResult<bool>> DeleteManyAsync(IEnumerable<string> ids)
        {
            ApiResult<bool> result = new ApiResult<bool>();

            using (HttpClient client = new HttpClient())
            {
                string json = JsonConvert.SerializeObject(ids);

                StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

                HttpRequestMessage request = new HttpRequestMessage
                {
                    Content = content,
                    Method = HttpMethod.Delete,
                    RequestUri = new Uri($"{Url}/policies")
                };

                HttpResponseMessage apiResult = await client.SendAsync(request);

                if (apiResult.IsSuccessStatusCode == false)
                {
                    result.Exception = new Exception(apiResult.ReasonPhrase);
                }

                return result;
            }
        }
    }
}
