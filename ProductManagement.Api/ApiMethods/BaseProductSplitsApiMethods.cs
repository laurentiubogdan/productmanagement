﻿using ProductManagement.Api.ApiMethodsInterfaces;
using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests.BaseProductSplits;
using ProductManagement.Api.Responses.BaseProductSplits;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProductManagement.Api.ApiMethods
{
    public class BaseProductSplitsApiMethods : IBaseProductSplitsApiMethods
    {
        private BackendUrlApi _apiSettings;
        private readonly IHttpClientFactory _clientFactory;

        public BaseProductSplitsApiMethods(BackendUrlApi apiSettings, IHttpClientFactory clientFactory)
        {
            _apiSettings = apiSettings;
            _clientFactory = clientFactory;
        }

        private string Url
        {
            get
            {
                return _apiSettings.BackendUrl;
            }
        }

        public async Task<ApiResult<BaseProductSplitsResponse>> GetAsync(string id)
        {
            ApiResult<BaseProductSplitsResponse> apiResponse = new ApiResult<BaseProductSplitsResponse>();
            HttpClient client = _clientFactory.CreateClient();

            string url = $"{Url}/baseSplits/{id}";

            HttpResponseMessage httpResponse = await client.GetAsync(url);

            if (httpResponse.IsSuccessStatusCode)
            {
                apiResponse.Result = await httpResponse.Content.ReadAsAsync<BaseProductSplitsResponse>();
            }
            else
            {
                apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
            }


            return apiResponse;
        }

        public async Task<ApiResult<BaseProductSplitsResponse>> CreateAsync(BaseProductSplitsCreateRequest request)
        {
            ApiResult<BaseProductSplitsResponse> apiResponse = new ApiResult<BaseProductSplitsResponse>();
            HttpClient client = _clientFactory.CreateClient();

            string url = $"{Url}/baseSplits";

            HttpResponseMessage httpResponse = await client.PostAsJsonAsync(url, request);

            if (httpResponse.IsSuccessStatusCode)
            {
                apiResponse.Result = await httpResponse.Content.ReadAsAsync<BaseProductSplitsResponse>();
            }
            else
            {
                apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
            }


            return apiResponse;
        }

        public async Task<ApiResult<BaseProductSplitsResponse>> UpdateAsync(BaseProductSplitsUpdateRequest request)
        {
            ApiResult<BaseProductSplitsResponse> apiResponse = new ApiResult<BaseProductSplitsResponse>();
            HttpClient client = _clientFactory.CreateClient();

            string url = $"{Url}/baseSplits/{request.Id}";

            HttpResponseMessage httpResponse = await client.PutAsJsonAsync(url, request);

            if (httpResponse.IsSuccessStatusCode)
            {
                apiResponse.Result = await httpResponse.Content.ReadAsAsync<BaseProductSplitsResponse>();
            }
            else
            {
                apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
            }


            return apiResponse;
        }
    }
}
