﻿using Newtonsoft.Json;
using ProductManagement.Api.ApiMethodsInterfaces;
using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Responses.Fee;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProductManagement.Api.ApiMethods
{
    public class FeeApiMethods : IFeeApiMethods
    {

        private BackendUrlApi _apiSettings;

        public FeeApiMethods(BackendUrlApi apiSettings)
        {
            _apiSettings = apiSettings;
        }

        private string Url
        {
            get
            {
                return _apiSettings.BackendUrl;
            }
        }

        public async Task<ApiResult<IEnumerable<FeeResponse>>> ListAsync(FilterRequest filterRequest)
        {
            ApiResult<IEnumerable<FeeResponse>> apiResponse = new ApiResult<IEnumerable<FeeResponse>>();

            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage httpResponse = new HttpResponseMessage();

                if (filterRequest == null)
                {
                    string url = $"{Url}/fees";

                    httpResponse = await client.GetAsync(url);
                } else
                {
                    string json = JsonConvert.SerializeObject(filterRequest);

                    StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

                    HttpRequestMessage request = new HttpRequestMessage
                    {
                        Content = content,
                        Method = HttpMethod.Get,
                        RequestUri = new Uri($"{Url}/fees/query")
                    };

                    httpResponse = await client.SendAsync(request);
                }

                if (httpResponse.IsSuccessStatusCode)
                {
                    apiResponse.Result = await httpResponse.Content.ReadAsAsync<IEnumerable<FeeResponse>>();
                }
                else
                {
                    apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
                }

                return apiResponse;
            }
        }
    }
}
