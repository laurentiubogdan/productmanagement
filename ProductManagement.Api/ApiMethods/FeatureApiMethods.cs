﻿using Newtonsoft.Json;
using ProductManagement.Api.ApiMethodsInterfaces;
using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Responses.Feature;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ProductManagement.Api.ApiMethods
{
    public class FeatureApiMethods : IFeatureApiMethods
    {
        private BackendUrlApi _apiSettings;

        public FeatureApiMethods(BackendUrlApi apiSettings)
        {
            _apiSettings = apiSettings;
        }

        private string Url
        {
            get
            {
                return _apiSettings.BackendUrl;
            }
        }

        public async Task<ApiResult<IEnumerable<FeatureResponse>>> ListAsync(QueryRequest queryRequest)
        {
            ApiResult<IEnumerable<FeatureResponse>> apiResponse = new ApiResult<IEnumerable<FeatureResponse>>();

            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage httpResponse = new HttpResponseMessage();

                if (queryRequest == null)
                {
                    string url = $"{Url}/features";

                    httpResponse = await client.GetAsync(url);
                }
                else
                {
                    string json = JsonConvert.SerializeObject(queryRequest);

                    StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

                    HttpRequestMessage request = new HttpRequestMessage
                    {
                        Content = content,
                        Method = HttpMethod.Post,
                        RequestUri = new Uri($"{Url}/features/query")
                    };

                    httpResponse = await client.SendAsync(request);
                }

                if (httpResponse.IsSuccessStatusCode)
                {
                    apiResponse.Result = await httpResponse.Content.ReadAsAsync<IEnumerable<FeatureResponse>>();
                }
                else
                {
                    apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
                }

                return apiResponse;
            }
        }

        public async Task<ApiResult<FeatureResponse>> GetAsync(string id)
        {
            ApiResult<FeatureResponse> apiResponse = new ApiResult<FeatureResponse>();

            using (HttpClient client = new HttpClient())
            {
                string url = $"{Url}/features/{id}";

                HttpResponseMessage httpResponse = await client.GetAsync(url);

                if(httpResponse.IsSuccessStatusCode)
                {
                    apiResponse.Result = await httpResponse.Content.ReadAsAsync<FeatureResponse>();
                } else
                {
                    apiResponse.Exception = new Exception(httpResponse.ReasonPhrase);
                }

                return apiResponse;
            }
        }

        public async Task<ApiResult<bool>> DeleteManyAsync(IEnumerable<string> ids)
        {
            ApiResult<bool> result = new ApiResult<bool>();

            using (HttpClient client = new HttpClient())
            {
                string json = JsonConvert.SerializeObject(ids);

                StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

                HttpRequestMessage request = new HttpRequestMessage
                {
                    Content = content,
                    Method = HttpMethod.Delete,
                    RequestUri = new Uri($"{Url}/features")
                };

                HttpResponseMessage apiResult = await client.SendAsync(request);

                if (apiResult.IsSuccessStatusCode == false)
                {
                    result.Exception = new Exception(apiResult.ReasonPhrase);
                }

                return result;
            }
        }
    }
}
