﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProductManagement.Domain.Helper_Objects;
using System.Collections.Generic;

namespace ProductManagement.Domain
{
    public class BaseProductSplits
    {
        public string Id { get; set; }

        public IEnumerable<Split> Splits { get; set; }

        public IEnumerable<Split> FilteredSplits { get; set; }

        public IEnumerable<JavaScriptRuleReference> Rules { get; set; }
    }
}
