﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ProductManagement.Domain.Helper_Objects;
using System.Collections.Generic;

namespace ProductManagement.Domain
{
    public class BaseProduct
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string UniqueCode { get; set; }

        public string ProductCode { get; set; }

        public string Description { get; set; }

        public string Funder { get; set; }

        public string Type { get; set; }

        public IEnumerable<string> DocumentationType { get; set; }

        public IEnumerable<string> PrimaryLoanPurpose { get; set; }

        public IEnumerable<Term> PrincipalInterestTerm { get; set; }

        public IEnumerable<Term> PrincipalInterestFeesTerm { get; set; }

        public IEnumerable<Term> PrepaidInterestTerm { get; set; }

        public IEnumerable<Term> InterestCapitalisedTerm { get; set; }

        public IEnumerable<Term> InterestOnlyTerm { get; set; }

        public IEnumerable<Term> VariableRateTerm { get; set; }

        public IEnumerable<Term> FixedRateTerm { get; set; }

        public IEnumerable<Interval> LoanAmount { get; set; }

        public IEnumerable<Interval> Lvr { get; set; }

        public IEnumerable<string> RepaymentMethods { get; set; }

        public IEnumerable<string> RepaymentFrequency { get; set; }

        public IEnumerable<string> StatementCycle { get; set; }

        public IEnumerable<string> SecurityPriority { get; set; }

        public int NumberOfSplits { get; set; }

        public int GenuineSavings { get; set; }

        public decimal BaseInterestRate { get; set; }

        public IEnumerable<string> LoanPurposes { get; set; }

        public IEnumerable<ProductFeatureLite> Features { get; set; }
    }
}
