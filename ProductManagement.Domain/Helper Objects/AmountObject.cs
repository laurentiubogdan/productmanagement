﻿using System.Collections.Generic;

namespace ProductManagement.Domain.Helper_Objects
{
    public class AmountObject
    {
        public int Amount { get; set; }
        public IEnumerable<JavascriptRule> Rules { get; set; }
    }
}
