﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ProductManagement.Domain.Helper_Objects
{
    public class JavaScriptRuleReference
    {
        public string Id { get; set; }
        public bool Active { get; set; }
    }
}
