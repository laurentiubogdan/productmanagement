﻿using System.Collections.Generic;

namespace ProductManagement.Domain.Helper_Objects
{
    public class ProductFeature
    {
        public Feature Feature { get; set; }
        public IEnumerable<JavascriptRule> Rules { get; set; }
        public string Status { get; set; }
    }
}
