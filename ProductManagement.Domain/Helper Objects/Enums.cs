﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace ProductManagement.Domain.Helper_Objects
{
    public static class Enums
    {
        public enum StatusEnum
        {
            Active = 1,
            Inactive = 2
        }
        public enum FeatureTypeEnum
        {
            General = 1,
            [Display(Name = "Home Loan")]
            HomeLoan = 2,
            Internet = 3
        }

        public enum FeeTypeEnum
        {
            General = 1,
            Government = 2,
            Application = 3,
            Bank = 4,
            LMI = 5
        }

        public enum PolicyTypeEnum
        {
            General = 1,
            [Display(Name = "Home Loan")]
            HomeLoan = 2,
            Insurance = 3,
            [Display(Name = "Debit Account")]
            DebitAccount = 4,
            [Display(Name = "Equipment Finance")]
            EquipmentFinance = 5
        }

        public enum ProductTypeEnum
        {
            [Display(Name = "Mortgage Loan")]
            MortgageLoan = 1,
            [Display(Name = "Personal Loan")]
            PersonalLoan = 2,
            [Display(Name = "Credit Card")]
            CreditCard = 3,
            [Display(Name = "Car Loan")]
            CarLoan = 4,
            [Display(Name = "Line Of Credit")]
            LineOfCredit = 5,
            [Display(Name = "Commercial Bill")]
            CommercialBill = 6,
            Lease = 7,
            [Display(Name = "Margin Loan")]
            MarginLoan = 8,
            Overdraft = 9,
            [Display(Name = "Reverse Mortgage")]
            ReverseMortgage = 10,
            [Display(Name = "Term Loan")]
            TermLoan = 11,
            [Display(Name = "Other Loan")]
            OtherLoan = 12
        }

        public enum PaymentTypeEnum
        {
            [Display(Name = "Principal & Interest")]
            PrincipalInterest = 1,
            [Display(Name = "Interest Only")]
            InterestOnly = 2
        }

        public enum InterestTypeEnum
        {
            Variable = 1,
            Fixed = 2
        }

        public enum TermFrequencyEnum
        {
            Days = 1,
            Weeks = 2,
            Months = 3,
            Years = 4
        }

        public enum FunderEnum
        {
            Advantedge = 1,
            Pepper = 2,
            Resimac = 3,
            [Display(Name = "Adelaide Bank")]
            AdelaideBank = 4,
            [Display(Name = "BC Securities")]
            BCSecurities = 5,
            Bendigo = 6,
            Firstmac = 7,
            LaTrobe = 8,
            Macquarie = 9,
            Origin = 10,
            RedZed = 11,
            Sintex = 12
        }

        public enum DocumentationType
        {
            [Display(Name = "Full Doc")]
            FullDoc = 1,
            [Display(Name = "Low Doc")]
            LowDoc = 2,
        }

        public enum PrimaryLoanPurpose
        {
            Business = 1,
            [Display(Name = "Investment Non Residential")]
            InvestmentNonResidential = 2,
            [Display(Name = "Investment Residential")]
            InvestmentResidential = 3,
            [Display(Name = "Owner Occupied")]
            OwnerOccupied = 4,
            Personal = 5
        }

        public enum RepaymentMethodsEnum
        {
            [Display(Name = "Australia Post")]
            AustraliaPost = 1,
            Bpay = 2,
            [Display(Name = "Credit Card")]
            CreditCard = 3,
            [Display(Name = "Direct Debit Existing Account")]
            DirectDebitExistingAccount = 4,
            [Display(Name = "Direct Debit New Account")]
            DirectDebitNewAccount = 5,
            [Display(Name = "Direct Salary Credit")]
            DirectSalaryCredit = 6,
            [Display(Name = "Funds Transfer")]
            FundsTransfer = 7,
            [Display(Name = "Lender Branch")]
            LenderBranch = 8,
            [Display(Name = "Staff Pay")]
            StaffPay = 9,
            Other = 10
        }

        public enum RepaymentFrequencyEnum
        {
            Yearly = 1,
            [Display(Name = "Half Yearly")]
            HalfYearly = 2,
            Seasonal = 3,
            Quarterly = 4,
            Monthly = 5,
            Fortnightly = 6,
            Weekly = 7,
            OneOff = 8,
            Daily = 9
        }

        public enum StatementCycleEnum
        {
            Yearly = 1,
            [Display(Name = "Half Yearly")]
            HalfYearly = 2,
            Quarterly = 3,
            Monthly = 4,
            Fortnightly = 5,
            Weekly = 6
        }

        public enum SecurityPriorityEnum
        {
            [Display(Name = "First Mortgage")]
            FirstMortgage = 1,
            [Display(Name = "Second Mortgage")]
            SecondMortgage = 2,
            [Display(Name = "Second After DSH")]
            SecondAfterDSH = 3,
            [Display(Name = "Third Mortgage")]
            ThirdMortgage = 4,
            [Display(Name = "Registered Mortgage")]
            RegisteredMortgage = 5,
            [Display(Name = "Unregistered Mortgage")]
            UnregisteredMortgage = 6
        }

        public enum RuleTypeEnum
        {
            [Display(Name = "Business Rule")]
            BusinessRule = 1,
            [Display(Name = "Primary Rule")]
            PrimaryRule = 2
        }

        public static string StringValueOfEnum(this Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            if (attributes.Length > 0)
            {
                return attributes[0].Description;
            }
            else
            {
                return value.ToString();
            }
        }
    }
}
