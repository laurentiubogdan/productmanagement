﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace ProductManagement.Domain.Helper_Objects
{
    public class ProductFeatureLite
    {
        public string Id { get; set; }
        public IEnumerable<JavascriptRule> Rules { get; set; }
        public string Status { get; set; }

    }
}
