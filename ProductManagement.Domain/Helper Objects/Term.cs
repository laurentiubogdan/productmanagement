﻿namespace ProductManagement.Domain.Helper_Objects
{
    public class Term
    {
        public int Value { get; set; }
        public string Frequency { get; set; }
    }
}
