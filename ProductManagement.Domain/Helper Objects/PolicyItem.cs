﻿using System.Collections.Generic;

namespace ProductManagement.Domain.Helper_Objects

{
    public class PolicyItem
    {
        public string Name { get; set; }
        public string Status { get; set; }
        public IEnumerable<JavascriptRule> ItemRules { get; set; }
    }
}
