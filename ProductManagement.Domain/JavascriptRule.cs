﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ProductManagement.Domain
{
    public class JavascriptRule
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Rule { get; set; }
        public string RuleType { get; set; }
    }
}
