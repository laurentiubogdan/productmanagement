﻿
namespace ProductManagement.Domain
{
    public class LoanPurpose
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
