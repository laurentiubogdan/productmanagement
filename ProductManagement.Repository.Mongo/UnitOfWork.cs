﻿using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using ProductManagement.Domain;
using ProductManagement.Domain.Helper_Objects;
using System;
using System.Collections;
using System.Collections.Generic;

namespace ProductManagement.Repository.Mongo
{
    public class UnitOfWork
    {
        public readonly IMongoCollection<Feature> Features;
        public readonly IMongoCollection<Fee> Fees;
        public readonly IMongoCollection<Policy> Policies;
        public readonly IMongoCollection<BaseProduct> BaseProducts;
        public readonly IMongoCollection<BaseProductSplits> BaseProductSplits;
        public readonly IMongoCollection<JavascriptRule> JavaScriptRules;
        public readonly IMongoCollection<LoanPurpose> LoanPurposes;

        public UnitOfWork(IConfiguration config)
        {
            Type[] types = { typeof(Feature), typeof(ProductFeatureLite), typeof(Fee), typeof(Policy), typeof(BaseProductSplits), typeof(JavascriptRule), typeof(LoanPurpose), };

            foreach (Type type in types)
            {
                var classMapDefinition = typeof(BsonClassMap<>);
                var classMapType = classMapDefinition.MakeGenericType(type);
                var classMap = (BsonClassMap)Activator.CreateInstance(classMapType);

                // Do custom initialization here, e.g. classMap.SetDiscriminator, AutoMap etc
                classMap.AutoMap();
                classMap.MapProperty("Id")
                    .SetIdGenerator(StringObjectIdGenerator.Instance)
                    .SetSerializer(new StringSerializer(BsonType.ObjectId));

                BsonClassMap.RegisterClassMap(classMap);
            }

            BsonClassMap.RegisterClassMap<BaseProduct>(
              cm =>
                  {
                      cm.AutoMap();
                      cm.MapProperty("Id")
                        .SetIdGenerator(StringObjectIdGenerator.Instance)
                        .SetSerializer(new StringSerializer(BsonType.ObjectId));
                      //cm.MapProperty(c => c.LoanPurposes)
                      //  .SetSerializer(new ArraySerializer<string>(new StringSerializer(BsonType.ObjectId)));
                  });


            // connecting to database
            MongoClient client = new MongoClient(config.GetConnectionString("ProductManagement"));
            IMongoDatabase database = client.GetDatabase("ProductManagement");

            Features = database.GetCollection<Feature>("Features");
            Fees = database.GetCollection<Fee>("Fees");
            Policies = database.GetCollection<Policy>("Policies");
            BaseProducts = database.GetCollection<BaseProduct>("BaseProducts");
            BaseProductSplits = database.GetCollection<BaseProductSplits>("BaseProductSplits");
            JavaScriptRules = database.GetCollection<JavascriptRule>("JavaScriptRules");
            LoanPurposes = database.GetCollection<LoanPurpose>("LoanPurposes");
        }
    }
}
