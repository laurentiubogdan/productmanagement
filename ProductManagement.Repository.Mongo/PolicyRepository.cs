﻿//using MongoDB.Driver;
//using ProductManagement.Api.Requests;
//using ProductManagement.Domain;
//using System.Collections.Generic;
//using System.Threading.Tasks;

//namespace ProductManagement.Repository.Mongo
//{
//    public class PolicyRepository : IPolicyRepository
//    {
//        private readonly UnitOfWork _unitOfWork;

//        public PolicyRepository(UnitOfWork unitOfWork)
//        {
//            _unitOfWork = unitOfWork;
//        }

//        public async Task<IEnumerable<Policy>> ListAsync(FilterRequest filterRequest)
//        {
//            if (filterRequest == null)
//            {
//                return await _unitOfWork.Policies.Find(policy => true).ToListAsync();
//            }
//            else
//            {
//                FilterDefinition<Policy> filter = Builders<Policy>.Filter
//                    .In(filterRequest.FilterProperty, filterRequest.FilterValues);

//                return await _unitOfWork.Policies.Find(filter).ToListAsync();
//            }
//        }

//        public async Task<Policy> GetAsync(string id)
//        {
//            return await _unitOfWork.Policies.Find(policy => policy.Id == id).FirstOrDefaultAsync();
//        }

//        public async Task<Policy> CreateAsync(Policy policy)
//        {
//            await _unitOfWork.Policies.InsertOneAsync(policy);
//            return policy;
//        }

//        public async Task<Policy> UpdateAsync(Policy policyIn)
//        {
//            await _unitOfWork.Policies.ReplaceOneAsync(policy => policy.Id == policyIn.Id, policyIn);
//            return policyIn;
//        }

//        public async Task DeleteAsync(Policy policyIn)
//        {
//            await _unitOfWork.Policies.DeleteOneAsync(policy => policy.Id == policyIn.Id);
//        }

//        public async Task DeleteManyAsync(IEnumerable<string> ids)
//        {
//            FilterDefinition<Policy> filter = Builders<Policy>.Filter.In(p => p.Id, ids);

//            await _unitOfWork.Policies.DeleteManyAsync(filter);
//        }
//    }
//}
