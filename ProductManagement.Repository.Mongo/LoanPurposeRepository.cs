﻿using MongoDB.Driver;
using ProductManagement.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Repository.Mongo
{
    public class LoanPurposeRepository : ILoanPurposeRepository
    {
        private UnitOfWork _unitOfWork;
        public LoanPurposeRepository(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IMongoCollection<LoanPurpose> BaseQuery()
        {
            return _unitOfWork.LoanPurposes;
        }

        public async Task<IEnumerable<LoanPurpose>> ListAsync()
        {
            return await BaseQuery().Find(loanPurposes => true).ToListAsync();
        }

        public async Task<LoanPurpose> GetAsync(string id)
        {
            return await _unitOfWork.LoanPurposes.Find(loanPurpose => loanPurpose.Id == id).FirstOrDefaultAsync();
        }

        public async Task<LoanPurpose> CreateAsync(LoanPurpose loanPurpose)
        {
            await _unitOfWork.LoanPurposes.InsertOneAsync(loanPurpose);
            return loanPurpose;
        }

        public async Task<LoanPurpose> UpdateAsync(LoanPurpose loanPurposeIn)
        {
            await _unitOfWork.LoanPurposes.ReplaceOneAsync(loanPurpose => loanPurpose.Id == loanPurposeIn.Id, loanPurposeIn);
            return loanPurposeIn;
        }

        public async Task DeleteAsync(LoanPurpose loanPurposeIn)
        {
            await _unitOfWork.LoanPurposes.DeleteOneAsync(loanPurpose => loanPurpose.Id == loanPurposeIn.Id);
        }

        public async Task DeleteAsync(string id)
        {
            await _unitOfWork.LoanPurposes.DeleteOneAsync(loanPurpose => loanPurpose.Id == id);
        }

        public async Task DeleteManyAsync(IEnumerable<string> ids)
        {
            FilterDefinition<LoanPurpose> filter = Builders<LoanPurpose>.Filter
                .In(p => p.Id, ids);

            await _unitOfWork.LoanPurposes.DeleteManyAsync(filter);
        }
    }
}
