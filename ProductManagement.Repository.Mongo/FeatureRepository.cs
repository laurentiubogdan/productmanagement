﻿using MongoDB.Bson;
using MongoDB.Driver;
using ProductManagement.Api.Requests;
using ProductManagement.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Repository.Mongo
{
    public class FeatureRepository : IFeatureRepository
    {
        private readonly UnitOfWork _unitOfWork;

        public FeatureRepository(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IMongoCollection<Feature> BaseQuery()
        {
            return _unitOfWork.Features;
        }

        public async Task<IEnumerable<Feature>> ListAsync()
        {
            return await BaseQuery().Find(feature => true).ToListAsync();
        }

        public async Task<Feature> GetAsync(string id)
        {
            return await _unitOfWork.Features.Find(feature => feature.Id == id).FirstOrDefaultAsync();
        }

        public async Task<Feature> CreateAsync(Feature feature)
        {
            await _unitOfWork.Features.InsertOneAsync(feature);
            return feature;
        }

        public async Task<IEnumerable<Feature>> CreateManyAsync(IEnumerable<Feature> features)
        {
            await _unitOfWork.Features.InsertManyAsync(features);
            return features;
        }

        public async Task<Feature> UpdateAsync(Feature featureIn)
        {
            await _unitOfWork.Features.ReplaceOneAsync(feature => feature.Id == featureIn.Id, featureIn);
            return featureIn;
        }

        public async Task DeleteAsync(Feature featureIn)
        {
            await _unitOfWork.Features.DeleteOneAsync(feature => feature.Id == featureIn.Id);
        }

        public async Task DeleteManyAsync(BsonArray featuresIds)
        {
            BsonDocument filter = new BsonDocument("_id", new BsonDocument("$in", featuresIds));
            await _unitOfWork.Features.DeleteManyAsync(filter);
        }
    }
}
