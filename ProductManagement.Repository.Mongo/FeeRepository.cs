﻿//using MongoDB.Bson;
//using MongoDB.Driver;
//using ProductManagement.Api.Requests;
//using ProductManagement.Domain;
//using System.Collections.Generic;
//using System.Threading.Tasks;

//namespace ProductManagement.Repository.Mongo
//{
//    public class FeeRepository : IFeeRepository
//    {
//        private readonly UnitOfWork _unitOfWork;

//        public FeeRepository(UnitOfWork unitOfWork)
//        {
//            _unitOfWork = unitOfWork;
//        }
//        public async Task<IEnumerable<Fee>> ListAsync(FilterRequest filterRequest)
//        {
//            if (filterRequest == null)
//            {
//                return await _unitOfWork.Fees.Find(fee => true).ToListAsync();
//            }
//            else
//            {
//                FilterDefinition<Fee> filter = Builders<Fee>.Filter
//                    .In(filterRequest.FilterProperty, filterRequest.FilterValues);

//                return await _unitOfWork.Fees.Find(filter).ToListAsync();
//            }
//        }

//        public async Task<Fee> GetAsync(string id)
//        {
//            return await _unitOfWork.Fees.Find(fee => fee.Id == id).FirstOrDefaultAsync();
//        }

//        public async Task<Fee> CreateAsync(Fee fee)
//        {
//            await _unitOfWork.Fees.InsertOneAsync(fee);
//            return fee;
//        }

//        public async Task<Fee> UpdateAsync(Fee feeIn)
//        {
//            await _unitOfWork.Fees.ReplaceOneAsync(Fee => Fee.Id == feeIn.Id, feeIn);
//            return feeIn;
//        }

//        public async Task DeleteAsync(Fee feeIn)
//        {
//            await _unitOfWork.Fees.DeleteOneAsync(Fee => Fee.Id == feeIn.Id);
//        }

//        public async Task DeleteManyAsync(BsonArray feesIds)
//        {
//            BsonDocument filter = new BsonDocument("_id", new BsonDocument("$in", feesIds));
//            await _unitOfWork.Fees.DeleteManyAsync(filter);
//        }
//    }
//}
