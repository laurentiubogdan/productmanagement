﻿using MongoDB.Driver;
using ProductManagement.Api.Requests;
using ProductManagement.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProductManagement.Repository.Mongo
{
    public class BaseProductSplitRepository : IBaseProductSplitRepository
    {
        private readonly UnitOfWork _unitOfWork;

        public BaseProductSplitRepository(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IMongoCollection<BaseProductSplits> BaseQuery()
        {
            return _unitOfWork.BaseProductSplits;
        }

        public async Task<IEnumerable<BaseProductSplits>> ListAsync()
        {
            return await BaseQuery().Find(baseProductSplit => true).ToListAsync();
        }

        public async Task<BaseProductSplits> CreateAsync(BaseProductSplits baseProductSplits)
        {
            await _unitOfWork.BaseProductSplits.InsertOneAsync(baseProductSplits);
            return baseProductSplits;
        }

        public async Task<BaseProductSplits> GetAsync(string id)
        {
            return await _unitOfWork.BaseProductSplits.Find(baseProdSplit => baseProdSplit.Id == id).FirstOrDefaultAsync();
        }

        public async Task<BaseProductSplits> UpdateSync(BaseProductSplits baseProdsplits)
        {
            await _unitOfWork.BaseProductSplits.ReplaceOneAsync(b => b.Id == baseProdsplits.Id, baseProdsplits);
            return baseProdsplits;
        }

        public async Task DeleteAsync(string id)
        {
            await _unitOfWork.BaseProductSplits.DeleteOneAsync(baseSplits => baseSplits.Id == id);
        }
    }
}
