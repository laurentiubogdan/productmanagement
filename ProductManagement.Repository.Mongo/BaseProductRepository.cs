﻿using MongoDB.Bson;
using MongoDB.Driver;
using ProductManagement.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Repository.Mongo
{
    public class BaseProductRepository : IBaseProductRepository
    {
        private UnitOfWork _unitOfWork;
        public BaseProductRepository(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IMongoCollection<BaseProduct> BaseQuery() {
           return _unitOfWork.BaseProducts;
        }

        public async Task<IEnumerable<BaseProduct>> ListAsync()
        {
            return await BaseQuery().Find(baseProduct => true).ToListAsync();
        }

        public async Task<BaseProduct> GetAsync(string id)
        {
            return await BaseQuery().Find(product => product.Id == id).FirstOrDefaultAsync();
        }

        public async Task<BaseProduct> CreateAsync(BaseProduct baseProduct)
        {
            await BaseQuery().InsertOneAsync(baseProduct);
            return baseProduct;
        }

        public async Task<BaseProduct> UpdateAsync(BaseProduct baseProductIn)
        {
            await BaseQuery().ReplaceOneAsync(product => product.Id == baseProductIn.Id, baseProductIn);
            return baseProductIn;
        }

        public async Task DeleteAsync(BaseProduct baseProductIn)
        {
            await BaseQuery().DeleteOneAsync(product => product.Id == baseProductIn.Id);
        }

        public async Task DeleteAsync(string id)
        {
            await BaseQuery().DeleteOneAsync(product => product.Id == id);
        }

        public async Task DeleteManyAsync(IEnumerable<string> ids)
        {
            FilterDefinition<BaseProduct> filter = Builders<BaseProduct>.Filter
                .In(p => p.Id, ids);

            await BaseQuery().DeleteManyAsync(filter);
        }
    }
}
