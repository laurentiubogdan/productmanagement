﻿using MongoDB.Driver;
using ProductManagement.Api.Requests;
using ProductManagement.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Repository.Mongo
{
    public class JavaScriptRuleRepository : IJavaScriptRuleRepository
    {
        private readonly UnitOfWork _unitOfWork;
        public JavaScriptRuleRepository(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IMongoCollection<JavascriptRule> BaseQuery()
        {
            return _unitOfWork.JavaScriptRules;
        }

        public async Task<IEnumerable<JavascriptRule>> ListAsync()
        {
            return await BaseQuery().Find(javascriptRule => true).ToListAsync();
        }

        public async Task<JavascriptRule> CreateAsync(JavascriptRule rule)
        {
            await _unitOfWork.JavaScriptRules.InsertOneAsync(rule);
            return rule;
        }

        public async Task<JavascriptRule> GetAsync(string id)
        {
            return await _unitOfWork.JavaScriptRules.Find(rule => rule.Id == id).FirstOrDefaultAsync();
        }

        public async Task DeleteAsync(string id)
        {
            await _unitOfWork.JavaScriptRules.DeleteOneAsync(Rules => Rules.Id == id);
        }

        public async Task DeleteManyAsync(IEnumerable<string> ids)
        {
            FilterDefinition<JavascriptRule> filter = Builders<JavascriptRule>.Filter
                .In(r => r.Id, ids);

            await _unitOfWork.JavaScriptRules.DeleteManyAsync(filter);
        }
    }
}
