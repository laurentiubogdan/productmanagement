﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Repository.MongoDB
{
    public class FeeRepository : IFeeRepository
    {
        private readonly UnitOfWork _unitOfWork;

        public FeeRepository(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Fee>> List()
        {
            return await _unitOfWork.fees.Find(Fee => true).ToListAsync();
        }
    }
}
