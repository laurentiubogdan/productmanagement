﻿using Microsoft.Extensions.Configuration;
using MongoDB.Driver;

namespace ProductManagement.Repository.MongoDB
{
    public class UnitOfWork
    {
        public UnitOfWork(IConfiguration config)
        {
            var client = new MongoClient(config.GetConnectionString("ProductManagement"));
            var database = client.GetDatabase("ProductManagement");

            features = database.GetCollection<Feature>("Features");
            policies = database.GetCollection<Policy>("Policies");
            fees = database.GetCollection<Fee>("Fees");

        }

        public IMongoCollection<Feature> features;
        public IMongoCollection<Policy> policies;
        public IMongoCollection<Fee> fees;

    }
}
