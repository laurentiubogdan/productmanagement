﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Repository.MongoDB
{
    public class PolicyRepository : IPolicyRepository
    {
        private UnitOfWork _unitOfWork;

        public PolicyRepository(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Policy>> Get()
        {
            return await _unitOfWork.policies.AsQueryable().ToListAsync();
        }
    }
}
