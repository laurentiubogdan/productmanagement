﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Repository.MongoDB
{
    public class FeatureRepository : IFeatureRepository
    {
        private UnitOfWork _unitOfWork;

        public FeatureRepository(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Feature>> Get()
        {
            return await _unitOfWork.features.Find(Feature => true).ToListAsync();
        }
    }
}
