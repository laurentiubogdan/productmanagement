﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProductManagement.Backend.BusinessLogic.BaseProductService;
using ProductManagement.Backend.BusinessLogic.FeatureService;
using ProductManagement.Backend.BusinessLogic.FeeService;
using ProductManagement.Backend.BusinessLogic.JavaScriptRuleService;
using ProductManagement.Backend.BusinessLogic.LoanPurposeService;
using ProductManagement.Backend.BusinessLogic.PolicyService;
using ProductManagement.Repository;
using ProductManagement.Repository.Mongo;

namespace ProductManagement.Backend
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<IISOptions>(options =>
            {
                options.ForwardClientCertificate = false;
            });

            services.AddSingleton<UnitOfWork>();

            services.AddScoped<IFeatureService, FeatureService>();
            services.AddScoped<IPolicyService, PolicyService>();
            services.AddScoped<IFeeService, FeeService>();
            services.AddScoped<IBaseProductService, BaseProductService>();
            services.AddScoped<IJavaScriptRuleService, JavaScriptRuleService>();
            services.AddScoped<ILoanPurposeService, LoanPurposeService>();

            services.AddScoped<IFeatureRepository, FeatureRepository>();
            //services.AddScoped<IPolicyRepository, PolicyRepository>();
            //services.AddScoped<IFeeRepository, FeeRepository>();
            services.AddScoped<IBaseProductRepository, BaseProductRepository>();
            services.AddScoped<IBaseProductSplitRepository, BaseProductSplitRepository>();
            services.AddScoped<IJavaScriptRuleRepository, JavaScriptRuleRepository>();
            services.AddScoped<ILoanPurposeRepository, LoanPurposeRepository>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action}/{id?}");
            });
        }
    }
}
