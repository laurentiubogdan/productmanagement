using Microsoft.AspNetCore.Mvc;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Requests.Policy;
using ProductManagement.Backend.BusinessLogic.PolicyService;
using ProductManagement.Backend.ExtensionMethods;
using ProductManagement.Domain;
using ProductManagement.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Backend.Controllers
{
    [ApiController]
    public class PolicyController : ControllerBase
    {
        private readonly IPolicyRepository _policyRepository;
        private readonly IPolicyService _policyService;

        public PolicyController(IPolicyRepository policyRepository, IPolicyService policyService)
        {
            _policyRepository = policyRepository;
            _policyService = policyService;
        }

        [HttpGet("/policies")]
        public async Task<IActionResult> List()
        {
            IEnumerable<Policy> policies = await _policyRepository.ListAsync();

            if (policies == null)
            {
                return NotFound();
            }

            return Ok(policies);
        }

        [HttpGet("/policies/query")]
        public async Task<IActionResult> List([FromBody] FilterRequest filterRequest)
        {
            IEnumerable<Policy> policies = await _policyRepository.ListAsync(filterRequest);

            if (policies == null)
            {
                return NotFound();
            }

            return Ok(policies);
        }

        [HttpGet("/policies/{id}")]
        public async Task<IActionResult> Get(string id)
        {
            Policy policy = await _policyRepository.GetAsync(id);

            if (policy == null)
            {
                return NotFound();
            }

            return Ok(policy.ToPolicyResponse());
        }

        [HttpPost("/policies")]
        public async Task<IActionResult> Create([FromBody] PolicyCreateRequest request)
        {

            if (request == null)
            {
                return BadRequest();
            }

            Policy policy = request.ToPolicyModel();

            await _policyRepository.CreateAsync(policy);

            return Ok(policy.ToPolicyResponse());
        }

        [HttpPut("/policies/{id}")]
        public async Task<IActionResult> Update([FromBody] PolicyUpdateRequest request, string id)
        {
            Policy policy = await _policyRepository.GetAsync(id);

            if (policy == null)
            {
                return NotFound();
            }

            request.Id = id;
            Policy policyIn = request.ToPolicyModel();

            await _policyRepository.UpdateAsync(policyIn);
            return Ok(policyIn.ToPolicyUpdateResponse());
        }

        [HttpDelete("/policies/{id}")]
        public async Task<IActionResult> Delete([FromRoute] string id)
        {
            Policy policy = await _policyRepository.GetAsync(id);

            if (policy == null)
            {
                return NotFound();
            }

            await _policyRepository.DeleteAsync(policy);

            return Ok();
        }

        [HttpDelete("/policies")]
        public async Task<IActionResult> Delete([FromBody] IEnumerable<string> request)
        {

            if (request == null)
            {
                return BadRequest();
            }

            bool response = await _policyService.DeletePolicies(request);

            if (response == false)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
