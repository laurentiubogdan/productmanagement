﻿using Microsoft.AspNetCore.Mvc;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Requests.BaseProductSplits;
using ProductManagement.Backend.BusinessLogic.BaseProductSplitService;
using ProductManagement.Backend.ExtensionMethods;
using ProductManagement.Domain;
using ProductManagement.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Backend.Controllers
{
    [ApiController]
    public class BaseProductSplitController : ControllerBase
    {
        private readonly IBaseProductSplitRepository _baseProductSplitRepository;
        private readonly IBaseProductSplitService _baseProductSplitService;

        public BaseProductSplitController(IBaseProductSplitRepository baseProductSplitRepository)
        {
            _baseProductSplitRepository = baseProductSplitRepository;
        }

        [HttpGet("/baseSplits")]
        public async Task<IActionResult> List()
        {
            IEnumerable<BaseProductSplits> baseProductSplits = await _baseProductSplitRepository.ListAsync();

            if (baseProductSplits == null)
            {
                return NotFound();
            }

            return Ok(baseProductSplits);
        }

        [HttpPost("/baseSplits/query")]
        public async Task<IActionResult> List(QueryRequest queryRequest)
        {
            IEnumerable<BaseProductSplits> baseProductSplits = await _baseProductSplitService.ListAsync(queryRequest);

            if (baseProductSplits == null)
            {
                return NotFound();
            }

            return Ok(baseProductSplits);
        }

        [HttpPost("/baseSplits")]
        public async Task<IActionResult> Create(BaseProductSplitsCreateRequest request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            BaseProductSplits baseProductSplits = request.ToBaseProductSplitsModel();
            await _baseProductSplitRepository.CreateAsync(baseProductSplits);
            return Ok(baseProductSplits.ToBaseProductSplitsResponse());
        }

        [HttpGet("/baseSplits/{id}")]
        public async Task<IActionResult> Get(string id)
        {
            BaseProductSplits baseProductSplits = await _baseProductSplitRepository.GetAsync(id);

            if (baseProductSplits == null)
            {
                return NotFound();
            }

            return Ok(baseProductSplits.ToBaseProductSplitsResponse());
        }

        [HttpPut("/baseSplits/{id}")]
        public async Task<IActionResult> Update(BaseProductSplitsUpdateRequest request, string id)
        {
            BaseProductSplits baseProdSplits = await _baseProductSplitRepository.GetAsync(id);

            if (baseProdSplits == null)
            {
                return BadRequest();
            }

            request.Id = id;
            BaseProductSplits baseProdSplitsIn = request.ToBaseProductSplitsModel();
            await _baseProductSplitRepository.UpdateSync(baseProdSplitsIn);

            return Ok(baseProdSplitsIn.ToBaseProductSplitsResponse());

        }
    }
}
