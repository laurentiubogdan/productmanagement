﻿using Microsoft.AspNetCore.Mvc;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Requests.Rule;
using ProductManagement.Backend.BusinessLogic.JavaScriptRuleService;
using ProductManagement.Backend.ExtensionMethods;
using ProductManagement.Domain;
using ProductManagement.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Backend.Controllers
{
    [ApiController]
    public class JavaScriptRuleController : ControllerBase
    {
        public readonly IJavaScriptRuleRepository _javaScriptRuleRepository;
        public readonly IJavaScriptRuleService _javaScriptRuleService;

        public JavaScriptRuleController(IJavaScriptRuleRepository javaScriptRuleRepository, IJavaScriptRuleService javaScriptRuleService)
        {
            _javaScriptRuleRepository = javaScriptRuleRepository;
            _javaScriptRuleService = javaScriptRuleService;
        }

        [HttpGet("/rule")]
        public async Task<IActionResult> List()
        {
            IEnumerable<JavascriptRule> rules = await _javaScriptRuleRepository.ListAsync();

            if (rules == null)
            {
                return NotFound();
            }

            return Ok(rules);
        }

        [HttpPost("/rule/query")]
        public async Task<IActionResult> List(QueryRequest queryRequest)
        {
            IEnumerable<JavascriptRule> javascriptRules = await _javaScriptRuleService.ListAsync(queryRequest);

            if (javascriptRules == null)
            {
                return NotFound();
            }

            return Ok(javascriptRules);
        }

        [HttpPost("/rule")]
        public async Task<IActionResult> Create(JavaScriptRuleCreateRequest request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            JavascriptRule rule = request.ToJavaScriptRule();
            await _javaScriptRuleRepository.CreateAsync(rule);
            return Ok(rule.ToJavascriptRuleResponse());
        }

        [HttpGet("/rule/{id}")]
        public async Task<IActionResult> Get(string id)
        {
            JavascriptRule rule = await _javaScriptRuleRepository.GetAsync(id);

            if (rule == null)
            {
                return NotFound();
            }

            return Ok(rule.ToJavascriptRuleResponse());
        }

        [HttpDelete("rule/{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            JavascriptRule rule = await _javaScriptRuleRepository.GetAsync(id);

            if (rule == null)
            {
                return NotFound();
            }

            await _javaScriptRuleRepository.DeleteAsync(id);

            return Ok();
        }

        [HttpDelete("/rule")]
        public async Task<IActionResult> Delete([FromBody]IEnumerable<string> rulesIds)
        {
            if (rulesIds == null)
            {
                return BadRequest();
            }

            bool response = await _javaScriptRuleService.DeleteRulesAsync(rulesIds);

            if (!response)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
