﻿using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Requests.Fee;
using ProductManagement.Backend.BusinessLogic.FeeService;
using ProductManagement.Backend.ExtensionMethods;
using ProductManagement.Domain;
using ProductManagement.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Backend.Controllers
{
    [ApiController]
    public class FeeController : ControllerBase
    {
        private readonly IFeeRepository _feeRepository;
        private readonly IFeeService _feeService;

        public FeeController(IFeeRepository feeRepository, IFeeService feeService)
        {
            _feeRepository = feeRepository;
            _feeService = feeService;
        }

        [HttpGet("/fees")]
        public async Task<IActionResult> List()
        {
            IEnumerable<Fee> fees = await _feeRepository.ListAsync();

            if (fees == null)
            {
                return NotFound();
            }

            return Ok(fees);
        }

        [HttpGet("/fees/query")]
        public async Task<IActionResult> List([FromBody] FilterRequest filterRequest)
        {
            IEnumerable<Fee> fees = await _feeRepository.ListAsync(filterRequest);

            if (fees == null)
            {
                return BadRequest();
            }

            return Ok(fees);
        }

        [HttpGet("/fees/{id}")]
        public async Task<IActionResult> Get(string id)
        {
            Fee fee = await _feeRepository.GetAsync(id);

            if (fee == null)
            {
                return NotFound();
            }

            return Ok(fee.ToFeeResponse());
        }

        [HttpPost("/fees")]
        public async Task<IActionResult> Create([FromBody] FeeCreateRequest request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            Fee fee = request.ToFeeModel();

            await _feeRepository.CreateAsync(fee);

            return Ok(fee.ToFeeResponse());
        }

        [HttpPut("/fees/{id}")]
        public async Task<IActionResult> Update([FromBody] FeeUpdateRequest request, string id)
        {
            Fee fee = await _feeRepository.GetAsync(id);

            if (fee == null)
            {
                return NotFound();
            }
            request.Id = id;
            Fee feeIn = request.ToFeeModel();

            await _feeRepository.UpdateAsync(feeIn);
            return Ok(feeIn.ToFeeUpdateResponse());
        }

        [HttpDelete("/fees/{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            Fee fee = await _feeRepository.GetAsync(id);

            if (fee == null)
            {
                return NotFound();
            }

            await _feeRepository.DeleteAsync(fee);
            return Ok();
        }


        [HttpDelete("/fees")]
        public async Task<IActionResult> Delete([FromBody] IEnumerable<string> fees)
        {
            if (fees == null)
            {
                return BadRequest();
            }

            BsonArray feesIds = await _feeService.DeleteFees(fees);

            if (feesIds == null)
            {
                return BadRequest();
            }

            return Ok();
        }
    }
}