﻿using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Requests.Feature;
using ProductManagement.Backend.BusinessLogic.FeatureService;
using ProductManagement.Backend.ExtensionMethods;
using ProductManagement.Domain;
using ProductManagement.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Backend.Controllers
{
    [ApiController]
    public class FeatureController : ControllerBase
    {
        private readonly IFeatureRepository _featureRepository;
        private readonly IFeatureService _featureService;

        public FeatureController(IFeatureRepository featureRepository, IFeatureService featureService)
        {
            _featureRepository = featureRepository;
            _featureService = featureService;
        }

        [HttpGet("/features")]
        public async Task<IActionResult> List()
        {
            IEnumerable<Feature> features = await _featureRepository.ListAsync();

            if (features == null)
            {
                return NotFound();
            }

            return Ok(features);
        }

        [HttpPost("features/query")]
        public async Task<IActionResult> List([FromBody] QueryRequest queryRequest)
        {
            IEnumerable<Feature> features = await _featureService.ListAsync(queryRequest);

            if (features == null)
            {
                return BadRequest();
            }

            return Ok(features);
        }

        [HttpGet("/features/{id}")]
        public async Task<IActionResult> Get(string id)
        {
            Feature feature = await _featureRepository.GetAsync(id);

            if (feature == null)
            {
                return NotFound();
            }

            return Ok(feature.ToFeatureResponse());
        }

        [HttpPost("/features")]
        public async Task<IActionResult> Create([FromBody] FeatureCreateRequest request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            Feature feature = request.ToFeatureModel();

            await _featureRepository.CreateAsync(feature);

            return Ok(feature.ToFeatureResponse());
        }

        [HttpPost("/features/many")]
        public async Task<IActionResult> CreateMany([FromBody] IEnumerable<FeatureCreateRequest> request)
        {
            if (request == null)
            {
                return BadRequest();
            }

            List<Feature> features = new List<Feature>();
            foreach (FeatureCreateRequest req in request)
            {
                if (req == null)
                {
                    return BadRequest();
                }
                features.Add(req.ToFeatureModel());
            }

            await _featureRepository.CreateManyAsync(features);

            return Ok(features);
        }

        [HttpPut("/features/{id}")]
        public async Task<IActionResult> Update([FromBody] FeatureUpdateRequest request, string id)
        {
            Feature feature = await _featureRepository.GetAsync(id);

            if (feature == null)
            {
                return NotFound();
            }

            request.Id = id;
            Feature featureIn = request.ToFeatureModel();

            await _featureRepository.UpdateAsync(featureIn);

            return Ok(featureIn.ToFeatureUpdateResponse());
        }

        [HttpDelete("/features/{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            Feature feature = await _featureRepository.GetAsync(id);

            if (feature == null)
            {
                return NotFound();
            }

            await _featureRepository.DeleteAsync(feature);

            return Ok();
        }

        [HttpDelete("/features")]
        public async Task<IActionResult> Delete([FromBody]IEnumerable<string> features)
        {
            if (features == null)
            {
                return BadRequest();
            }

            BsonArray featuresIds = await _featureService.DeleteFeaturesAsync(features);

            if (featuresIds == null)
            {
                return BadRequest();
            }

            return Ok();
        }
    }
}
