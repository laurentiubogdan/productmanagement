﻿using Microsoft.AspNetCore.Mvc;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Requests.BaseProduct;
using ProductManagement.Api.Responses.Feature;
using ProductManagement.Backend.BusinessLogic.BaseProductService;
using ProductManagement.Backend.ExtensionMethods;
using ProductManagement.Domain;
using ProductManagement.Domain.Helper_Objects;
using ProductManagement.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement.Backend.Controllers
{
    [ApiController]
    public class BaseProductController : ControllerBase
    {
        private readonly IBaseProductRepository _baseProductRepository;
        private readonly IBaseProductService _baseProductService;
        public BaseProductController(IBaseProductRepository baseProductRepository, IBaseProductService baseProductService)
        {
            _baseProductRepository = baseProductRepository;
            _baseProductService = baseProductService;
        }

        [HttpGet("/base")]
        public async Task<IActionResult> List()
        {
            IEnumerable<BaseProduct> baseProducts = await _baseProductRepository.ListAsync();
            if (baseProducts == null)
            {
                return NotFound();
            }

            return Ok(baseProducts);
        }

        [HttpPost("/base/query")]
        public async Task<IActionResult> List(QueryRequest queryRequest)
        {
            IEnumerable<BaseProduct> baseProducts = await _baseProductService.ListAsync(queryRequest);

            if (baseProducts == null)
            {
                return NotFound();
            }

            return Ok(baseProducts);
        }

        [HttpGet("/base/{id}")]
        public async Task<IActionResult> Get(string id)
        {
            BaseProduct baseProduct = await _baseProductRepository.GetAsync(id);

            if (baseProduct == null)
            {
                return NotFound();
            }

            return Ok(baseProduct.ToBaseProductResponse());
        }

        [HttpPost("/base")]
        public async Task<IActionResult> Create(BaseProductCreateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            int number = 1;

            while (number < 100)
            {
                BaseProduct baseProduct = request.ToBaseProductModel();

            await _baseProductRepository.CreateAsync(baseProduct);
            number++;
            }
            return Ok();
        }

        [HttpPut("/base/{id}")]
        public async Task<IActionResult> Update(BaseProductUpdateRequest request, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            BaseProduct baseProduct = await _baseProductRepository.GetAsync(id);

            if (baseProduct == null)
            {
                return NotFound();
            }

            BaseProduct baseProductIn = request.ToBaseProductModel();
            baseProductIn.Id = id;

            await _baseProductRepository.UpdateAsync(baseProductIn);

            return Ok(baseProductIn.ToBaseProductResponse());
        }

        [HttpDelete("/base/{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            BaseProduct baseProduct = await _baseProductRepository.GetAsync(id);

            if (baseProduct == null)
            {
                return NotFound();
            }

            await _baseProductRepository.DeleteAsync(baseProduct);

            return Ok();
        }

        [HttpDelete("/base")]
        public async Task<IActionResult> Delete([FromBody] IEnumerable<string> baseProductsIds)
        {
            if (baseProductsIds == null)
            {
                return BadRequest();
            }

            bool response = await _baseProductService.DeleteBaseProductsAsync(baseProductsIds);

            if (!response)
            {
                return NotFound();
            }

            return NoContent();
        }

        [HttpGet("/base/{baseProductid}/loanPurposes")]
        public async Task<IActionResult> ListLoanPurposes(string baseProductid)
        {

            BaseProduct baseProduct = await _baseProductRepository.GetAsync(baseProductid);

            if (baseProduct == null)
            {
                return NotFound();
            }

            IEnumerable<LoanPurpose> loanPurposes = await _baseProductService.ListLoanPurposesAsync(baseProduct);

            return Ok(loanPurposes.Select(p => p.ToLoanPurposeResponse()));
        }

        [HttpPost("/base/{baseProductid}/loanPurposes/query")]
        public async Task<IActionResult> ListLoanPurposes(string baseProductid, QueryRequest queryRequest)
        {

            BaseProduct baseProduct = await _baseProductRepository.GetAsync(baseProductid);

            if (baseProduct == null)
            {
                return NotFound();
            }

            IEnumerable<LoanPurpose> loanPurposes = await _baseProductService.ListLoanPurposesAsync(baseProduct, queryRequest);

            return Ok(loanPurposes.Select(p => p.ToLoanPurposeResponse()));
        }

        [HttpGet("/base/{baseProductid}/features")]
        public async Task<IActionResult> ListFeatures(string baseProductid)
        {
            BaseProduct baseProduct = await _baseProductRepository.GetAsync(baseProductid);

            if (baseProduct == null)
            {
                return NotFound();
            }

            IEnumerable<ProductFeature> features = await _baseProductService.ListFeaturesAsync(baseProduct);

            return Ok(features.Select(p => p.ToProductFeatureResponse()));
        }

        [HttpPost("/base/{baseProductid}/features/query")]
        public async Task<IActionResult> ListFeatures(string baseProductid, QueryRequest queryRequest)
        {

            BaseProduct baseProduct = await _baseProductRepository.GetAsync(baseProductid);

            if (baseProduct == null)
            {
                return NotFound();
            }

            IEnumerable<ProductFeature> productFeatures = await _baseProductService.ListFeaturesAsync(baseProduct, queryRequest);

            return Ok(productFeatures.Select(p => p.ToProductFeatureResponse()));
        }
    }
}
