﻿using Microsoft.AspNetCore.Mvc;
using ProductManagement.Api.Requests;
using ProductManagement.Api.Requests.LoanPurpose;
using ProductManagement.Api.Responses.LoanPurpose;
using ProductManagement.Backend.BusinessLogic.LoanPurposeService;
using ProductManagement.Backend.ExtensionMethods;
using ProductManagement.Domain;
using ProductManagement.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement.Backend.Controllers
{
    [ApiController]
    public class LoanPurposeController : ControllerBase
    {
        private readonly ILoanPurposeRepository _loanPurposeRepository;
        private readonly ILoanPurposeService _loanPurposeService;

        public LoanPurposeController(ILoanPurposeRepository loanPurposeRepository, ILoanPurposeService loanPurposeService)
        {
            _loanPurposeRepository = loanPurposeRepository;
            _loanPurposeService = loanPurposeService;
        }

        [HttpGet("/loanPurposes")]
        public async Task<IActionResult> List()
        {
            IEnumerable<LoanPurpose> loanPurposes = await _loanPurposeRepository.ListAsync();
            if (loanPurposes == null)
            {
                return NotFound();
            }

            IEnumerable<LoanPurposeResponse> loanPurposesResponse = loanPurposes.Select(p => p.ToLoanPurposeResponse());

            return Ok(loanPurposesResponse);
        }

        [HttpPost("/loanPurposes/query")]
        public async Task<IActionResult> List(QueryRequest queryRequest)
        {
            IEnumerable<LoanPurpose> loanPurposes = await _loanPurposeService.ListAsync(queryRequest);

            if (loanPurposes == null)
            {
                return NotFound();
            }

            return Ok(loanPurposes);
        }

        [HttpGet("/loanPurposes/{id}")]
        public async Task<IActionResult> Get(string id)
        {
            LoanPurpose loanPurpose = await _loanPurposeRepository.GetAsync(id);

            if (loanPurpose == null)
            {
                return NotFound();
            }

            return Ok(loanPurpose.ToLoanPurposeResponse());
        }

        [HttpPost("/loanPurposes")]
        public async Task<IActionResult> Create(LoanPurposeCreateRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            int number = 1;

            while(number < 200000)
            {
                LoanPurpose loanPurpose = request.ToLoanPurposeModel();

                await _loanPurposeRepository.CreateAsync(loanPurpose);
                number++;
            }

            return Ok();
        }

        [HttpPut("/loanPurposes/{id}")]
        public async Task<IActionResult> Update(LoanPurposeUpdateRequest request, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            LoanPurpose loanPurpose = await _loanPurposeRepository.GetAsync(id);

            if (loanPurpose == null)
            {
                return NotFound();
            }

            LoanPurpose loanPurposeIn = request.ToLoanPurposeModel();
            loanPurposeIn.Id = id;

            await _loanPurposeRepository.UpdateAsync(loanPurposeIn);

            return Ok(loanPurposeIn.ToLoanPurposeResponse());
        }

        [HttpDelete("/loanPurposes/{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            LoanPurpose loanPurpose = await _loanPurposeRepository.GetAsync(id);

            if (loanPurpose == null)
            {
                return NotFound();
            }

            await _loanPurposeRepository.DeleteAsync(loanPurpose);

            return Ok();
        }

        [HttpDelete("/loanPurposes")]
        public async Task<IActionResult> Delete([FromBody] IEnumerable<string> loanPurposesIds)
        {
            if (loanPurposesIds == null)
            {
                return BadRequest();
            }

            bool response = await _loanPurposeService.DeleteLoanPurposesAsync(loanPurposesIds);

            if (!response)
            {
                return NotFound();
            }
            return NoContent();
        }
    }
}
