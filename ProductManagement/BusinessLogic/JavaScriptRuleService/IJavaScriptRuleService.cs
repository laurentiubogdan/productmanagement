﻿using ProductManagement.Api.Requests;
using ProductManagement.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Backend.BusinessLogic.JavaScriptRuleService
{
    public interface IJavaScriptRuleService
    {
        Task<IEnumerable<JavascriptRule>> ListAsync(QueryRequest queryRequest);
        Task<bool> DeleteRulesAsync(IEnumerable<string> ids);
    }
}
