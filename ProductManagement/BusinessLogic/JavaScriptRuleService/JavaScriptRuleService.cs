﻿using MongoDB.Bson;
using MongoDB.Driver;
using ProductManagement.Api.Requests;
using ProductManagement.Domain;
using ProductManagement.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement.Backend.BusinessLogic.JavaScriptRuleService
{
    public class JavaScriptRuleService : IJavaScriptRuleService
    {
        private readonly IJavaScriptRuleRepository _javaScriptRuleRepository;

        public async Task<IEnumerable<JavascriptRule>> ListAsync(QueryRequest queryRequest)
        {
            IFindFluent<JavascriptRule, JavascriptRule> baseQuery = _javaScriptRuleRepository.BaseQuery().Find(baseProduct => true);
            if (queryRequest.FilterRequests != null)
            {
                FilterDefinitionBuilder<JavascriptRule> builder = Builders<JavascriptRule>.Filter;
                List<FilterDefinition<JavascriptRule>> inFilterDefinitions = new List<FilterDefinition<JavascriptRule>>();
                List<FilterDefinition<JavascriptRule>> containsFilterDefinitions = new List<FilterDefinition<JavascriptRule>>();

                foreach (FilterRequest request in queryRequest.FilterRequests)
                {
                    string property = request.Property;
                    string operatorr = request.Operator;

                    if (operatorr == "In")
                    {
                        FilterDefinition<JavascriptRule> filterDefinition = builder.In(request.Property, request.Values);
                        inFilterDefinitions.Add(filterDefinition);
                    }
                    else if (operatorr == "Contains")
                    {
                        FilterDefinition<JavascriptRule> filterDefinition = builder.Regex(request.Property, BsonRegularExpression.Create($"/{request.Values.First()}/i"));
                        containsFilterDefinitions.Add(filterDefinition);
                    }
                }

                FilterDefinition<JavascriptRule> inOrFilterDefinition = builder.Or(FilterDefinition<JavascriptRule>.Empty);
                FilterDefinition<JavascriptRule> containsOrFilterDefinition = builder.Or(FilterDefinition<JavascriptRule>.Empty);

                if (inFilterDefinitions.Count() > 0)
                {
                    inOrFilterDefinition = builder.Or(inFilterDefinitions);
                }

                if (containsFilterDefinitions.Count() > 0)
                {
                    containsOrFilterDefinition = builder.Or(containsFilterDefinitions);
                }

                FilterDefinition<JavascriptRule> mainFilterDefinition = builder.And(inOrFilterDefinition, containsOrFilterDefinition);

                baseQuery = _javaScriptRuleRepository.BaseQuery().Find(mainFilterDefinition);
            }

            if (queryRequest.ProjectionRequest != null)
            {
                var fields = queryRequest.ProjectionRequest.ProjectionFields;
                ProjectionDefinition<JavascriptRule> projection = Builders<JavascriptRule>.Projection.Include(fields.First());

                foreach (string field in fields.Skip(1))
                {
                    projection = projection.Include(field);
                }
                baseQuery = baseQuery.Project<JavascriptRule>(projection);
            }
            return await baseQuery.Limit(queryRequest.Limit).Skip(queryRequest.Skip).ToListAsync();
        }

        public JavaScriptRuleService(IJavaScriptRuleRepository javaScriptRuleRepository)
        {
            _javaScriptRuleRepository = javaScriptRuleRepository;
        }

        public async Task<bool> DeleteRulesAsync(IEnumerable<string> ids)
        {
            List<string> ruleIds = new List<string>();

            foreach (string id in ruleIds)
            {
                JavascriptRule rule = await _javaScriptRuleRepository.GetAsync(id);

                if (rule == null)
                {
                    return false;
                }

                ruleIds.Add(rule.Id);
            }

            await _javaScriptRuleRepository.DeleteManyAsync(ruleIds);
            return true;
        }
    }
}
