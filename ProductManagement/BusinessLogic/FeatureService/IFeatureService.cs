﻿using MongoDB.Bson;
using ProductManagement.Api.Requests;
using ProductManagement.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Backend.BusinessLogic.FeatureService
{
    public interface IFeatureService
    {
        Task<IEnumerable<Feature>> ListAsync(QueryRequest queryRequest);
        Task<BsonArray> DeleteFeaturesAsync(IEnumerable<string> features);
    }
}
