﻿using MongoDB.Bson;
using MongoDB.Driver;
using ProductManagement.Api.Requests;
using ProductManagement.Domain;
using ProductManagement.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement.Backend.BusinessLogic.FeatureService
{
    public class FeatureService : IFeatureService
    {
        private readonly IFeatureRepository _featureRepository;

        public FeatureService(IFeatureRepository featureRepository)
        {
            _featureRepository = featureRepository;
        }

        public async Task<IEnumerable<Feature>> ListAsync(QueryRequest queryRequest)
        {
            IFindFluent<Feature, Feature> baseQuery = _featureRepository.BaseQuery().Find(feature => true);
            if (queryRequest.FilterRequests != null)
            {
                FilterDefinitionBuilder<Feature> builder = Builders<Feature>.Filter;
                List<FilterDefinition<Feature>> inFilterDefinitions = new List<FilterDefinition<Feature>>();
                List<FilterDefinition<Feature>> containsFilterDefinitions = new List<FilterDefinition<Feature>>();

                foreach (FilterRequest request in queryRequest.FilterRequests)
                {
                    string property = request.Property;
                    string operatorr = request.Operator;

                    if (operatorr == "In")
                    {
                        FilterDefinition<Feature> filterDefinition = builder.In(request.Property, request.Values);
                        inFilterDefinitions.Add(filterDefinition);
                    }
                    else if (operatorr == "Contains")
                    {
                        FilterDefinition<Feature> filterDefinition = builder.Regex(request.Property, BsonRegularExpression.Create($"/{request.Values.First()}/i"));
                        containsFilterDefinitions.Add(filterDefinition);
                    }
                }

                FilterDefinition<Feature> inOrFilterDefinition = builder.Or(FilterDefinition<Feature>.Empty);
                FilterDefinition<Feature> containsOrFilterDefinition = builder.Or(FilterDefinition<Feature>.Empty);

                if (inFilterDefinitions.Count() > 0)
                {
                    inOrFilterDefinition = builder.Or(inFilterDefinitions);
                }

                if (containsFilterDefinitions.Count() > 0)
                {
                    containsOrFilterDefinition = builder.Or(containsFilterDefinitions);
                }

                FilterDefinition<Feature> mainFilterDefinition = builder.And(inOrFilterDefinition, containsOrFilterDefinition);

                baseQuery = _featureRepository.BaseQuery().Find(mainFilterDefinition);
            }

            if (queryRequest.SortRequests != null)
            {
                var sortDefinitionBuilder = Builders<Feature>.Sort;
                List<SortDefinition<Feature>> sortDefinitions = new List<SortDefinition<Feature>>();

                foreach (SortRequest sortRequest in queryRequest.SortRequests)
                {
                    var field = sortRequest.Field;
                    var direction = sortRequest.Direction;
                    if (direction == "asc")
                    {
                        var sortDefinition = sortDefinitionBuilder.Ascending(field);
                        sortDefinitions.Add(sortDefinition);
                    }
                    else
                    {
                        var sortDefinition = sortDefinitionBuilder.Descending(field);
                        sortDefinitions.Add(sortDefinition);
                    }
                }
                var mainSortDefinition = sortDefinitionBuilder.Combine(sortDefinitions);
                baseQuery = baseQuery.Sort(mainSortDefinition);
            }

            if (queryRequest.ProjectionRequest != null)
            {
                var fields = queryRequest.ProjectionRequest.ProjectionFields;
                ProjectionDefinition<Feature> projection = Builders<Feature>.Projection.Include(fields.First());

                foreach (string field in fields.Skip(1))
                {
                    projection = projection.Include(field);
                }
                baseQuery = baseQuery.Project<Feature>(projection);
            }
            return await baseQuery.Limit(queryRequest.Limit).Skip(queryRequest.Skip).ToListAsync();
        }

        public async Task<BsonArray> DeleteFeaturesAsync(IEnumerable<string> features)
        {
            BsonArray featureArray = new BsonArray();

            foreach (string feat in features)
            {
                Feature feature = await _featureRepository.GetAsync(feat);

                if (feature == null)
                {
                    return null;
                }

                featureArray.Add(ObjectId.Parse(feature.Id));
            }

            await _featureRepository.DeleteManyAsync(featureArray);

            return featureArray;
        }
    }
}
