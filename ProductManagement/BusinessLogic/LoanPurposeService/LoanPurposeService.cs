﻿using MongoDB.Bson;
using MongoDB.Driver;
using ProductManagement.Api.Requests;
using ProductManagement.Domain;
using ProductManagement.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement.Backend.BusinessLogic.LoanPurposeService
{
    public class LoanPurposeService : ILoanPurposeService
    {
        private readonly ILoanPurposeRepository _loanPurposeRepository;

        public LoanPurposeService(ILoanPurposeRepository loanPurposeRepository)
        {
            _loanPurposeRepository = loanPurposeRepository;
        }

        public async Task<IEnumerable<LoanPurpose>> ListAsync(QueryRequest queryRequest)
        {
            IFindFluent<LoanPurpose, LoanPurpose> baseQuery = _loanPurposeRepository.BaseQuery().Find(baseProduct => true);
            if (queryRequest.FilterRequests != null)
            {
                FilterDefinitionBuilder<LoanPurpose> builder = Builders<LoanPurpose>.Filter;
                List<FilterDefinition<LoanPurpose>> inFilterDefinitions = new List<FilterDefinition<LoanPurpose>>();
                List<FilterDefinition<LoanPurpose>> containsFilterDefinitions = new List<FilterDefinition<LoanPurpose>>();

                foreach (FilterRequest request in queryRequest.FilterRequests)
                {
                    string property = request.Property;
                    string operatorr = request.Operator;

                    if (operatorr == "In")
                    {
                        FilterDefinition<LoanPurpose> filterDefinition = builder.In(request.Property, request.Values);
                        inFilterDefinitions.Add(filterDefinition);
                    }
                    else if (operatorr == "Contains")
                    {
                        FilterDefinition<LoanPurpose> filterDefinition = builder.Regex(request.Property, BsonRegularExpression.Create($"/{request.Values.First()}/i"));
                        containsFilterDefinitions.Add(filterDefinition);
                    }
                }

                FilterDefinition<LoanPurpose> inOrFilterDefinition = builder.Or(FilterDefinition<LoanPurpose>.Empty);
                FilterDefinition<LoanPurpose> containsOrFilterDefinition = builder.Or(FilterDefinition<LoanPurpose>.Empty);

                if (inFilterDefinitions.Count() > 0)
                {
                    inOrFilterDefinition = builder.Or(inFilterDefinitions);
                }

                if (containsFilterDefinitions.Count() > 0)
                {
                    containsOrFilterDefinition = builder.Or(containsFilterDefinitions);
                }

                FilterDefinition<LoanPurpose> mainFilterDefinition = builder.And(inOrFilterDefinition, containsOrFilterDefinition);

                baseQuery = _loanPurposeRepository.BaseQuery().Find(mainFilterDefinition);
            }

            if (queryRequest.SortRequests != null)
            {
                var sortDefinitionBuilder = Builders<LoanPurpose>.Sort;
                List<SortDefinition<LoanPurpose>> sortDefinitions = new List<SortDefinition<LoanPurpose>>();

                foreach (SortRequest sortRequest in queryRequest.SortRequests)
                {
                    var field = sortRequest.Field;
                    var direction = sortRequest.Direction;
                    if (direction == "asc")
                    {
                        var sortDefinition = sortDefinitionBuilder.Ascending(field);
                        sortDefinitions.Add(sortDefinition);
                    }
                    else
                    {
                        var sortDefinition = sortDefinitionBuilder.Descending(field);
                        sortDefinitions.Add(sortDefinition);
                    }
                }
                var mainSortDefinition = sortDefinitionBuilder.Combine(sortDefinitions);
                baseQuery = baseQuery.Sort(mainSortDefinition);
            }

            if (queryRequest.ProjectionRequest != null)
            {
                var fields = queryRequest.ProjectionRequest.ProjectionFields;
                ProjectionDefinition<LoanPurpose> projection = Builders<LoanPurpose>.Projection.Include(fields.First());

                foreach (string field in fields.Skip(1))
                {
                    projection = projection.Include(field);
                }
                baseQuery = baseQuery.Project<LoanPurpose>(projection);
            }
            return await baseQuery.Limit(queryRequest.Limit).Skip(queryRequest.Skip).ToListAsync();
        }

        public async Task<bool> DeleteLoanPurposesAsync(IEnumerable<string> ids)
        {
            List<string> loanPurposesIds = new List<string>();

            foreach (string id in ids)
            {
                LoanPurpose loanPurpose = await _loanPurposeRepository.GetAsync(id);

                if (loanPurpose == null)
                {
                    return false;
                }

                loanPurposesIds.Add(loanPurpose.Id);
            }

            await _loanPurposeRepository.DeleteManyAsync(loanPurposesIds);

            return true;
        }
    }
}
