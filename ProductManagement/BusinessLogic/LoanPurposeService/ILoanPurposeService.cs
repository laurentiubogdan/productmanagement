﻿using ProductManagement.Api.Requests;
using ProductManagement.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Backend.BusinessLogic.LoanPurposeService
{
    public interface ILoanPurposeService
    {
        Task<IEnumerable<LoanPurpose>> ListAsync(QueryRequest queryRequest);
        Task<bool> DeleteLoanPurposesAsync(IEnumerable<string> ids);
    }
}
