﻿using ProductManagement.Api.Requests;
using ProductManagement.Domain;
using ProductManagement.Domain.Helper_Objects;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Backend.BusinessLogic.BaseProductService
{
    public interface IBaseProductService
    {
        Task<IEnumerable<BaseProduct>> ListAsync(QueryRequest queryRequest);
        Task<bool> DeleteBaseProductsAsync(IEnumerable<string> ids);
        Task<IEnumerable<LoanPurpose>> ListLoanPurposesAsync(BaseProduct baseProduct, QueryRequest queryRequest = null);
        Task<IEnumerable<ProductFeature>> ListFeaturesAsync(BaseProduct baseProduct, QueryRequest queryRequest = null);
    }
}
