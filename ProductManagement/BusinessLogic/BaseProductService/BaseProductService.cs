﻿using MongoDB.Bson;
using MongoDB.Driver;
using ProductManagement.Api.Requests;
using ProductManagement.Backend.BusinessLogic.LoanPurposeService;
using ProductManagement.Domain;
using ProductManagement.Domain.Helper_Objects;
using ProductManagement.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement.Backend.BusinessLogic.BaseProductService
{
    public class BaseProductService : IBaseProductService
    {
        private readonly IBaseProductRepository _baseProductRepository;
        private readonly ILoanPurposeService _loanPurposeService;
        private readonly IFeatureRepository _featureRepository;


        public BaseProductService(IBaseProductRepository baseProductRepository, ILoanPurposeService loanPurposeService, IFeatureRepository featureRepository)
        {
            _baseProductRepository = baseProductRepository;
            _loanPurposeService = loanPurposeService;
            _featureRepository = featureRepository;
        }

        public async Task<IEnumerable<BaseProduct>> ListAsync(QueryRequest queryRequest)
        {
            IFindFluent<BaseProduct, BaseProduct> baseQuery = _baseProductRepository.BaseQuery().Find(baseProduct => true);
            if (queryRequest.FilterRequests != null)
            {
                FilterDefinitionBuilder<BaseProduct> builder = Builders<BaseProduct>.Filter;
                List<FilterDefinition<BaseProduct>> inFilterDefinitions = new List<FilterDefinition<BaseProduct>>();
                List<FilterDefinition<BaseProduct>> containsFilterDefinitions = new List<FilterDefinition<BaseProduct>>();

                foreach (FilterRequest request in queryRequest.FilterRequests)
                {
                    string property = request.Property;
                    string operatorr = request.Operator;

                    if (operatorr == "In")
                    {
                        FilterDefinition<BaseProduct> filterDefinition = builder.In(request.Property, request.Values);
                        inFilterDefinitions.Add(filterDefinition);
                    }
                    else if (operatorr == "Contains")
                    {
                        FilterDefinition<BaseProduct> filterDefinition = builder.Regex(request.Property, BsonRegularExpression.Create($"/{request.Values.First()}/i"));
                        containsFilterDefinitions.Add(filterDefinition);
                    }
                }

                FilterDefinition<BaseProduct> inOrFilterDefinition = builder.Or(FilterDefinition<BaseProduct>.Empty);
                FilterDefinition<BaseProduct> containsOrFilterDefinition = builder.Or(FilterDefinition<BaseProduct>.Empty);

                if (inFilterDefinitions.Count() > 0)
                {
                    inOrFilterDefinition = builder.Or(inFilterDefinitions);
                }

                if (containsFilterDefinitions.Count() > 0)
                {
                    containsOrFilterDefinition = builder.Or(containsFilterDefinitions);
                }

                FilterDefinition<BaseProduct> mainFilterDefinition = builder.And(inOrFilterDefinition, containsOrFilterDefinition);

                baseQuery = _baseProductRepository.BaseQuery().Find(mainFilterDefinition);
            }

            if (queryRequest.SortRequests != null)
            {
                var sortDefinitionBuilder = Builders<BaseProduct>.Sort;
                List<SortDefinition<BaseProduct>> sortDefinitions = new List<SortDefinition<BaseProduct>>();

                foreach (SortRequest sortRequest in queryRequest.SortRequests)
                {
                    var field = sortRequest.Field;
                    var direction = sortRequest.Direction;
                    if (direction == "asc")
                    {
                        var sortDefinition = sortDefinitionBuilder.Ascending(field);
                        sortDefinitions.Add(sortDefinition);
                    }
                    else
                    {
                        var sortDefinition = sortDefinitionBuilder.Descending(field);
                        sortDefinitions.Add(sortDefinition);
                    }
                }
                var mainSortDefinition = sortDefinitionBuilder.Combine(sortDefinitions);
                baseQuery = baseQuery.Sort(mainSortDefinition);
            }

            if (queryRequest.ProjectionRequest != null)
            {
                var fields = queryRequest.ProjectionRequest.ProjectionFields;
                ProjectionDefinition<BaseProduct> projection = Builders<BaseProduct>.Projection.Include(fields.First());

                foreach (string field in fields.Skip(1))
                {
                    projection = projection.Include(field);
                }
                baseQuery = baseQuery.Project<BaseProduct>(projection);
            }
            return await baseQuery.Limit(queryRequest.Limit).Skip(queryRequest.Skip).ToListAsync();
        }

        public async Task<bool> DeleteBaseProductsAsync(IEnumerable<string> ids)
        {
            List<string> baseproductsIds = new List<string>();

            foreach (string id in ids)
            {
                BaseProduct baseProduct = await _baseProductRepository.GetAsync(id);

                if (baseProduct == null)
                {
                    return false;
                }

                baseproductsIds.Add(baseProduct.Id);
            }

            await _baseProductRepository.DeleteManyAsync(baseproductsIds);

            return true;
        }

        public async Task<IEnumerable<LoanPurpose>> ListLoanPurposesAsync(BaseProduct baseProduct, QueryRequest queryRequest = null)
        {
            QueryRequest request = new QueryRequest
            {
                FilterRequests = Enumerable.Repeat(new FilterRequest
                {
                    Property = "Id",
                    Operator = "In",
                    Values = baseProduct.LoanPurposes
                }, 1)
            };

            if (queryRequest != null)
            {
                request.ProjectionRequest = queryRequest.ProjectionRequest;
                request.SortRequests = queryRequest.SortRequests;
                request.Limit = queryRequest.Limit;
                request.Skip = queryRequest.Skip;
            }

            return await _loanPurposeService.ListAsync(request);
        }

        public async Task<IEnumerable<ProductFeature>> ListFeaturesAsync(BaseProduct baseProduct, QueryRequest queryRequest = null)
        {
            List<ProductFeature> productFeatures = new List<ProductFeature>();

            if(baseProduct.Features != null)
            {
                foreach (ProductFeatureLite featureLite in baseProduct.Features)
                {
                    Feature feature = await _featureRepository.GetAsync(featureLite.Id);

                    ProductFeature productFeature = new ProductFeature
                    {
                        Feature = feature,
                        Rules = featureLite.Rules,
                        Status = featureLite.Status
                    };

                    productFeatures.Add(productFeature);
                }
            }
            
            return productFeatures;
        }
    }
}
