﻿using ProductManagement.Domain;
using ProductManagement.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Backend.BusinessLogic.PolicyService
{
    public class PolicyService : IPolicyService
    {
        private readonly IPolicyRepository _policyRepository;

        public PolicyService(IPolicyRepository policyRepository)
        {
            _policyRepository = policyRepository;
        }

        public async Task<bool> DeletePolicies(IEnumerable<string> ids)
        {
            List<string> policiesIds = new List<string>();

            foreach (string id in ids)
            {
                Policy policy = await _policyRepository.GetAsync(id);

                if (policy == null)
                {
                    return false;
                }

                policiesIds.Add(policy.Id);
            }

                await _policyRepository.DeleteManyAsync(policiesIds);

            return true;
        }
    }
}
