﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Backend.BusinessLogic.PolicyService
{
    public interface IPolicyService
    {
        Task<bool> DeletePolicies(IEnumerable<string> policiesIds);
    }
}
