﻿using MongoDB.Bson;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Backend.BusinessLogic.FeeService
{
    public interface IFeeService
    {
        Task<BsonArray> DeleteFees(IEnumerable<string> fees);
    }
}
