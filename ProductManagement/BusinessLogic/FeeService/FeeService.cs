﻿using MongoDB.Bson;
using ProductManagement.Domain;
using ProductManagement.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Backend.BusinessLogic.FeeService
{
    public class FeeService : IFeeService
    {
        private readonly IFeeRepository _feeRepository;

        public FeeService(IFeeRepository feeRepository)
        {
            _feeRepository = feeRepository;
        }

        public async Task<BsonArray> DeleteFees(IEnumerable<string> fees)
        {
            BsonArray feeArray = new BsonArray();

            foreach (string fee in fees)
            {
                Fee feeIn = await _feeRepository.GetAsync(fee);

                if (feeIn == null)
                {
                    return null;
                }

                feeArray.Add(ObjectId.Parse(feeIn.Id));
            }

            await _feeRepository.DeleteManyAsync(feeArray);

            return feeArray;
        }
    }
}
