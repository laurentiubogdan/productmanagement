﻿using ProductManagement.Api.Requests;
using ProductManagement.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Backend.BusinessLogic.BaseProductSplitService
{
    public interface IBaseProductSplitService
    {
        Task<IEnumerable<BaseProductSplits>> ListAsync(QueryRequest queryRequest);
        //Task<bool> DeleteBaseProductsAsync(IEnumerable<string> ids);
    }
}
