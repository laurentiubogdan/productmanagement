﻿using MongoDB.Bson;
using MongoDB.Driver;
using ProductManagement.Api.Requests;
using ProductManagement.Domain;
using ProductManagement.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement.Backend.BusinessLogic.BaseProductSplitService
{
    public class BaseProductSplitService : IBaseProductSplitService
    {
        private readonly IBaseProductSplitRepository _baseProductSplitRepository;

        public BaseProductSplitService(IBaseProductSplitRepository baseProductSplitRepository)
        {
            _baseProductSplitRepository = baseProductSplitRepository;
        }

        public async Task<IEnumerable<BaseProductSplits>> ListAsync(QueryRequest queryRequest)
        {
            IFindFluent<BaseProductSplits, BaseProductSplits> baseQuery = _baseProductSplitRepository.BaseQuery().Find(baseProduct => true);
            if (queryRequest.FilterRequests != null)
            {
                FilterDefinitionBuilder<BaseProductSplits> builder = Builders<BaseProductSplits>.Filter;
                List<FilterDefinition<BaseProductSplits>> inFilterDefinitions = new List<FilterDefinition<BaseProductSplits>>();
                List<FilterDefinition<BaseProductSplits>> containsFilterDefinitions = new List<FilterDefinition<BaseProductSplits>>();

                foreach (FilterRequest request in queryRequest.FilterRequests)
                {
                    string property = request.Property;
                    string operatorr = request.Operator;

                    if (operatorr == "In")
                    {
                        FilterDefinition<BaseProductSplits> filterDefinition = builder.In(request.Property, request.Values);
                        inFilterDefinitions.Add(filterDefinition);
                    }
                    else if (operatorr == "Contains")
                    {
                        FilterDefinition<BaseProductSplits> filterDefinition = builder.Regex(request.Property, BsonRegularExpression.Create($"/{request.Values.First()}/i"));
                        containsFilterDefinitions.Add(filterDefinition);
                    }
                }

                FilterDefinition<BaseProductSplits> inOrFilterDefinition = builder.Or(FilterDefinition<BaseProductSplits>.Empty);
                FilterDefinition<BaseProductSplits> containsOrFilterDefinition = builder.Or(FilterDefinition<BaseProductSplits>.Empty);

                if (inFilterDefinitions.Count() > 0)
                {
                    inOrFilterDefinition = builder.Or(inFilterDefinitions);
                }

                if (containsFilterDefinitions.Count() > 0)
                {
                    containsOrFilterDefinition = builder.Or(containsFilterDefinitions);
                }

                FilterDefinition<BaseProductSplits> mainFilterDefinition = builder.And(inOrFilterDefinition, containsOrFilterDefinition);

                baseQuery = _baseProductSplitRepository.BaseQuery().Find(mainFilterDefinition);
            }

            if (queryRequest.ProjectionRequest != null)
            {
                var fields = queryRequest.ProjectionRequest.ProjectionFields;
                ProjectionDefinition<BaseProductSplits> projection = Builders<BaseProductSplits>.Projection.Include(fields.First());

                foreach (string field in fields.Skip(1))
                {
                    projection = projection.Include(field);
                }
                baseQuery = baseQuery.Project<BaseProductSplits>(projection);
            }
            return await baseQuery.Limit(queryRequest.Limit).Skip(queryRequest.Skip).ToListAsync();
        }
    }
}
