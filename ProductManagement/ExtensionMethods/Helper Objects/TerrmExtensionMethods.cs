﻿using ProductManagement.Api.Requests.Terms;
using ProductManagement.Domain.Helper_Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductManagement.Backend.ExtensionMethods.Helper_Objects
{
    public static class TerrmExtensionMethods
    {
        public static IEnumerable<Term> ToTermModel(this TermUpdateRequest request)
        {
            if (request.Values.Count() == 0 || request.Frequency == null)
            {
                return Enumerable.Empty<Term>();
            }

            List<Term> terms = new List<Term>();

            foreach (int value in request.Values)
            {
                Term term = new Term
                {
                    Value = value,
                    Frequency = request.Frequency.ToString()
                };

                terms.Add(term);
            }
            return terms;
        }
    }
}
