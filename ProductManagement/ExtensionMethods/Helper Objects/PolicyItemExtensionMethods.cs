﻿using ProductManagement.Api.Responses.Helper_Objects;
using ProductManagement.Domain.Helper_Objects;
using System.Linq;

namespace ProductManagement.Backend.ExtensionMethods.Helper_Objects
{
    public static class PolicyItemExtensionMethods
    {
        public static PolicyItemResponse ToPolicyItemResponse (this PolicyItem model)
        {
            return new PolicyItemResponse
            {
                Name = model.Name,
                Status = model.Status,
                ItemRules = model.ItemRules.Select(p => p.ToJavascriptRuleResponse())
            };
        }
    }
}
