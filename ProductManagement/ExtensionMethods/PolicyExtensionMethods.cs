﻿using ProductManagement.Api.Requests.Policy;
using ProductManagement.Api.Responses.Policy;
using ProductManagement.Api.Responses.Rule;
using ProductManagement.Backend.ExtensionMethods.Helper_Objects;
using ProductManagement.Domain;
using ProductManagement.Domain.Helper_Objects;
using System.Linq;

namespace ProductManagement.Backend.ExtensionMethods
{
    public static class PolicyExtensionMethods
    {
        public static Policy ToPolicyModel(this PolicyCreateRequest request)
        {
            return new Policy
            {
                Name = request.Name,
                Description = request.Description,
                Type = request.Type.ToString(),
                Status = request.Status.ToString(),
                Items = request.Items,
                Rules = request.Rules
            };
        }

        public static Policy ToPolicyModel(this PolicyUpdateRequest request)
        {
            return new Policy
            {
                Id = request.Id,
                Name = request.Name,
                Description = request.Description,
                Type = request.Type.ToString(),
                Status = request.Status.ToString(),
                Items = request.Items,
                Rules = request.Rules
            };
        }

        public static PolicyResponse ToPolicyResponse(this Policy model)
        {
            return new PolicyResponse
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                Type = model.Type,
                Status = model.Status,
                Items = model.Items.Select(p => p.ToPolicyItemResponse()),
                Rules = model.Rules.Select(p => p.ToJavascriptRuleResponse()),
            };
        }

        public static ProductPolicyResponse ToProductPolicyResponse(this ProductPolicy helperModel)
        {
            return new ProductPolicyResponse
            {
                Policy = helperModel.Policy.ToPolicyResponse(),
                Rules = (helperModel.Rules == null) ? Enumerable.Empty<JavaScriptRuleResponse>() : helperModel.Rules.Select(p => p.ToJavascriptRuleResponse()),
                Status = helperModel.Status
            };
        }

        public static PolicyUpdateResponse ToPolicyUpdateResponse(this Policy model)
        {
            return new PolicyUpdateResponse
            {
                Name = model.Name,
                Description = model.Description,
                Type = model.Type,
                Status = model.Status,
                Items = model.Items,
                Rules = model.Rules
            };
        }
    }
}
