﻿using ProductManagement.Api.Requests.Rule;
using ProductManagement.Api.Responses.Rule;
using ProductManagement.Domain;

namespace ProductManagement.Backend.ExtensionMethods
{
    public static class JavascriptRuleExtensionMethods
    {
        public static JavaScriptRuleResponse ToJavascriptRuleResponse(this JavascriptRule model)
        {
            return new JavaScriptRuleResponse
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                Rule = model.Rule,
                RuleType = model.RuleType
            };
        }

        public static JavascriptRule ToJavaScriptRule(this JavaScriptRuleCreateRequest request)
        {
            return new JavascriptRule
            {
                Name = request.Name,
                Description = request.Description,
                Rule = request.Rule,
                RuleType = request.RuleType
            };
        }
    }
}
