﻿using MongoDB.Bson;
using ProductManagement.Api.Helpers;
using ProductManagement.Api.Requests.BaseProduct;
using ProductManagement.Api.Responses.BaseProduct;
using ProductManagement.Backend.ExtensionMethods.Helper_Objects;
using ProductManagement.Domain;
using ProductManagement.Domain.Helper_Objects;
using System;
using System.Linq;

namespace ProductManagement.Backend.ExtensionMethods
{
    public static class BaseProductExtensionMethods
    {
        public static BaseProduct ToBaseProductModel(this BaseProductCreateRequest request)
        {
            return new BaseProduct
            {
                Id = ObjectId.GenerateNewId().ToString(),
                Name = request.Name,
                ProductCode = request.ProductCode,
                UniqueCode = request.Name.GenerateUniqueCode(),
                Description = request.Description,
                Funder = request.Funder.ToString(),
                Type = request.Type.ToString(),
                DocumentationType = Enumerable.Empty<string>(),
                PrimaryLoanPurpose = Enumerable.Empty<string>(),
                PrincipalInterestTerm = Enumerable.Empty<Term>(),
                InterestOnlyTerm = Enumerable.Empty<Term>(),
                PrepaidInterestTerm = Enumerable.Empty<Term>(),
                InterestCapitalisedTerm = Enumerable.Empty<Term>(),
                PrincipalInterestFeesTerm = Enumerable.Empty<Term>(),
                VariableRateTerm = Enumerable.Empty<Term>(),
                FixedRateTerm = Enumerable.Empty<Term>(),
                LoanAmount = Enumerable.Empty<Interval>(),
                Lvr = Enumerable.Empty<Interval>(),
                RepaymentMethods = Enumerable.Empty<string>(),
                RepaymentFrequency = Enumerable.Empty<string>(),
                StatementCycle = Enumerable.Empty<string>(),
                SecurityPriority = Enumerable.Empty<string>(),
                NumberOfSplits = 0,
                GenuineSavings = 0,
                BaseInterestRate = 0,
                LoanPurposes = new string[] { },
                Features = Enumerable.Empty<ProductFeatureLite>()
            };
        }

        public static BaseProduct ToBaseProductModel(this BaseProductUpdateRequest request)
        {
            return new BaseProduct
            {
                Name = request.Name,
                ProductCode = request.ProductCode,
                UniqueCode = request.Name.GenerateUniqueCode(),
                Description = request.Description,
                Funder = request.Funder.ToString(),
                Type = request.Type.ToString(),
                DocumentationType = request.DocumentationType == null ? Enumerable.Empty<string>() : request.DocumentationType.Select(p => p.ToString()),
                PrimaryLoanPurpose = request.PrimaryLoanPurpose == null ? Enumerable.Empty<string>() : request.PrimaryLoanPurpose.Select(p => p.ToString()),
                PrincipalInterestTerm = request.PrincipalInterestTerm.ToTermModel(),
                InterestOnlyTerm = request.InterestOnlyTerm.ToTermModel(),
                PrepaidInterestTerm = request.PrepaidInterestTerm.ToTermModel(),
                InterestCapitalisedTerm = request.InterestCapitalisedTerm.ToTermModel(),
                PrincipalInterestFeesTerm = request.PrincipalInterestFeesTerm.ToTermModel(),
                VariableRateTerm = request.VariableRateTerm.ToTermModel(),
                FixedRateTerm = request.FixedRateTerm.ToTermModel(),
                LoanAmount = request.LoanAmount.ToArrayIfNull(),
                Lvr = request.Lvr.ToArrayIfNull(),
                RepaymentMethods = request.RepaymentMethods == null ? Enumerable.Empty<string>() : request.RepaymentMethods.Select(p => p.ToString()),
                RepaymentFrequency = request.RepaymentFrequency == null ? Enumerable.Empty<string>() : request.RepaymentFrequency.Select(p => p.ToString()),
                StatementCycle = request.StatementCycle == null ? Enumerable.Empty<string>() : request.StatementCycle.Select(p => p.ToString()),
                SecurityPriority = request.SecurityPriority == null ? Enumerable.Empty<string>() : request.SecurityPriority.Select(p => p.ToString()),
                NumberOfSplits = request.NumberOfSplits,
                GenuineSavings = request.GenuineSavings,
                BaseInterestRate = request.BaseInterestRate,
                LoanPurposes = request.LoanPurposes == null ? Enumerable.Empty<string>() : request.LoanPurposes.Select(p => p.ToString()),
                //LoanPurposes = request.LoanPurposes ?? (new string[] { }),
                Features = request.Features
            };
        }

        public static BaseProductResponse ToBaseProductResponse(this BaseProduct response)
        {
            return new BaseProductResponse
            {
                Id = response.Id,
                Name = response.Name,
                ProductCode = response.ProductCode,
                UniqueCode = response.Name.GenerateUniqueCode(),
                Description = response.Description,
                Funder = response.Funder,
                Type = response.Type,
                DocumentationType = response.DocumentationType,
                PrimaryLoanPurpose = response.PrimaryLoanPurpose,
                PrincipalInterestTerm = response.PrincipalInterestTerm,
                InterestOnlyTerm = response.InterestOnlyTerm,
                PrepaidInterestTerm = response.PrepaidInterestTerm,
                InterestCapitalisedTerm = response.InterestCapitalisedTerm,
                PrincipalInterestFeesTerm = response.PrincipalInterestFeesTerm,
                VariableRateTerm = response.VariableRateTerm,
                FixedRateTerm = response.FixedRateTerm,
                LoanAmount = response.LoanAmount,
                Lvr = response.Lvr,
                RepaymentMethods = response.RepaymentMethods,
                RepaymentFrequency = response.RepaymentFrequency,
                StatementCycle = response.StatementCycle,
                SecurityPriority = response.SecurityPriority,
                NumberOfSplits = response.NumberOfSplits,
                GenuineSavings = response.GenuineSavings,
                BaseInterestRate = response.BaseInterestRate,
                LoanPurposes = response.LoanPurposes,
                Features = response.Features
            };
        }
    }
}
