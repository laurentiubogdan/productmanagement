﻿using ProductManagement.Api.Requests.Fee;
using ProductManagement.Api.Responses.Fee;
using ProductManagement.Domain;

namespace ProductManagement.Backend.ExtensionMethods
{
    public static class FeeExtensionMethods
    {
        public static Fee ToFeeModel(this FeeCreateRequest request)
        {
            return new Fee
            {
                Name = request.Name,
                Description = request.Description,
                Type = request.Type.ToString(),
                Status = request.Status.ToString(),
                Rules = request.Rules,
                Amounts = request.Amounts
            };
        }

        public static Fee ToFeeModel(this FeeUpdateRequest request)
        {
            return new Fee
            {
                Id = request.Id,
                Name = request.Name,
                Description = request.Description,
                Type = request.Type.ToString(),
                Status = request.Status.ToString(),
                Rules = request.Rules,
                Amounts = request.Amounts
            };
        }

        public static FeeResponse ToFeeResponse(this Fee model)
        {
            return new FeeResponse
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                Type = model.Type,
                Status = model.Status,
                Rules = model.Rules,
                Amounts = model.Amounts
            };
        }

        public static FeeUpdateResponse ToFeeUpdateResponse(this Fee model)
        {
            return new FeeUpdateResponse
            {
                Name = model.Name,
                Description = model.Description,
                Type = model.Type,
                Status = model.Status,
                Rules = model.Rules,
                Amounts = model.Amounts
            };
        }
    }
}
