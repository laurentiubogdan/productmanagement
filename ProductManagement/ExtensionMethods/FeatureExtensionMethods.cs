﻿using ProductManagement.Api.Requests.Feature;
using ProductManagement.Api.Responses.Feature;
using ProductManagement.Api.Responses.Rule;
using ProductManagement.Domain;
using ProductManagement.Domain.Helper_Objects;
using System.Linq;

namespace ProductManagement.Backend.ExtensionMethods
{
    public static class FeatureExtensionMethods
    {

        public static Feature ToFeatureModel(this FeatureCreateRequest request)
        {
            return new Feature
            {
                Name = request.Name,
                Description = request.Description,
                Type = request.Type.ToString(),
                Status = request.Status.ToString(),
                Rules = request.Rules
            };
        }

        public static Feature ToFeatureModel(this FeatureUpdateRequest request)
        {
            return new Feature
            {
                Id = request.Id,
                Name = request.Name,
                Description = request.Description,
                Type = request.Type.ToString(),
                Status = request.Status.ToString(),
                Rules = request.Rules
            };
        }

        public static FeatureResponse ToFeatureResponse(this Feature model)
        {
            return new FeatureResponse
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                Type = model.Type,
                Status = model.Status,
                Rules = model.Rules.Select(p => p.ToJavascriptRuleResponse())
            };
        }

        public static ProductFeatureResponse ToProductFeatureResponse(this ProductFeature helperModel)
        {
            return new ProductFeatureResponse
            {
                Feature = helperModel.Feature.ToFeatureResponse(),
                Rules = (helperModel.Rules == null) ? Enumerable.Empty<JavaScriptRuleResponse>() : helperModel.Rules.Select(p => p.ToJavascriptRuleResponse()),
                Status = helperModel.Status
            };
        }

        public static FeatureUpdateResponse ToFeatureUpdateResponse(this Feature model)
        {
            return new FeatureUpdateResponse
            {
                Name = model.Name,
                Description = model.Description,
                Type = model.Type,
                Status = model.Status,
                Rules = model.Rules
            };
        }
    }
}
