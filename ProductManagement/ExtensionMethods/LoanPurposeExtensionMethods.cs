﻿using MongoDB.Bson;
using ProductManagement.Api.Requests.LoanPurpose;
using ProductManagement.Api.Responses.LoanPurpose;
using ProductManagement.Domain;

namespace ProductManagement.Backend.ExtensionMethods
{
    public static class LoanPurposeExtensionMethods
    {
        public static LoanPurpose ToLoanPurposeModel(this LoanPurposeCreateRequest request)
        {
            return new LoanPurpose
            {
                Id = ObjectId.GenerateNewId().ToString(),
                Name = request.Name,
            };
        }

        public static LoanPurpose ToLoanPurposeModel(this LoanPurposeUpdateRequest request)
        {
            return new LoanPurpose
            {
                Name = request.Name,
            };
        }

        public static LoanPurposeResponse ToLoanPurposeResponse(this LoanPurpose response)
        {
            return new LoanPurposeResponse
            {
                Id = response.Id.ToString(),
                Name = response.Name,
            };
        }
    }
}
