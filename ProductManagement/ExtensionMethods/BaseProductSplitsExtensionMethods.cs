﻿using ProductManagement.Api.Requests.BaseProductSplits;
using ProductManagement.Api.Responses.BaseProductSplits;
using ProductManagement.Domain;

namespace ProductManagement.Backend.ExtensionMethods
{
    public static class BaseProductSplitsExtensionMethods
    {
        public static BaseProductSplits ToBaseProductSplitsModel(this BaseProductSplitsCreateRequest request)
        {
            return new BaseProductSplits
            {
                Id = request.Id,
                Splits = request.Splits,
                FilteredSplits = request.FilteredSplits,
                Rules = request.Rules
            };
        }

        public static BaseProductSplits ToBaseProductSplitsModel(this BaseProductSplitsUpdateRequest request)
        {
            return new BaseProductSplits
            {
                Id = request.Id,
                Splits = request.Splits,
                FilteredSplits = request.FilteredSplits,
                Rules = request.Rules
            };
        }

        public static BaseProductSplitsResponse ToBaseProductSplitsResponse(this BaseProductSplits model)
        {
            return new BaseProductSplitsResponse
            {
                Id = model.Id,
                Splits = model.Splits,
                FilteredSplits = model.FilteredSplits,
                Rules = model.Rules
            };
        }
    }
}
