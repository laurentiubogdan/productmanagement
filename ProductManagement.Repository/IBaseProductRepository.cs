﻿using MongoDB.Driver;
using ProductManagement.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Repository
{
    public interface IBaseProductRepository
    {
        IMongoCollection<BaseProduct> BaseQuery();
        Task<IEnumerable<BaseProduct>> ListAsync();
        Task<BaseProduct> GetAsync(string id);
        Task<BaseProduct> CreateAsync(BaseProduct baseProduct);
        Task<BaseProduct> UpdateAsync(BaseProduct baseProductIn);
        Task DeleteAsync(BaseProduct baseProduct);
        Task DeleteManyAsync(IEnumerable<string> ids);
    }
}
