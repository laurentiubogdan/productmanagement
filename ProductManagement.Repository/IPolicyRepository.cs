﻿using ProductManagement.Api.Requests;
using ProductManagement.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Repository
{
    public interface IPolicyRepository
    {
        Task<IEnumerable<Policy>> ListAsync(FilterRequest filterRequest = null);
        Task<Policy> GetAsync(string id);
        Task<Policy> CreateAsync(Policy policy);
        Task<Policy> UpdateAsync(Policy policyIn);
        Task DeleteAsync(Policy policy);
        Task DeleteManyAsync(IEnumerable<string> ids);
    }
}
