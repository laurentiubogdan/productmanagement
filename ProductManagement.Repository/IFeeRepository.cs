﻿using MongoDB.Bson;
using ProductManagement.Api.Requests;
using ProductManagement.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Repository
{
    public interface IFeeRepository
    {
        Task<IEnumerable<Fee>> ListAsync(FilterRequest filterRequest = null);
        Task<Fee> GetAsync(string id);
        Task<Fee> CreateAsync(Fee fee);
        Task<Fee> UpdateAsync(Fee feeIn);
        Task DeleteAsync(Fee feeIn);
        Task DeleteManyAsync(BsonArray feesIds);
    }
}
