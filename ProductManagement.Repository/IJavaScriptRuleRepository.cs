﻿using MongoDB.Driver;
using ProductManagement.Api.Requests;
using ProductManagement.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Repository
{
    public interface IJavaScriptRuleRepository
    {
        IMongoCollection<JavascriptRule> BaseQuery();
        Task<IEnumerable<JavascriptRule>> ListAsync();
        Task<JavascriptRule> CreateAsync(JavascriptRule rule);
        Task<JavascriptRule> GetAsync(string id);
        Task DeleteAsync(string id);
        Task DeleteManyAsync(IEnumerable<string> ids);
    }
}
