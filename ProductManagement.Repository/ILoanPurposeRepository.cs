﻿using MongoDB.Driver;
using ProductManagement.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Repository
{
    public interface ILoanPurposeRepository
    {
        IMongoCollection<LoanPurpose> BaseQuery();
        Task<IEnumerable<LoanPurpose>> ListAsync();
        Task<LoanPurpose> GetAsync(string id);
        Task<LoanPurpose> CreateAsync(LoanPurpose loanPurpose);
        Task<LoanPurpose> UpdateAsync(LoanPurpose loanPurpose);
        Task DeleteAsync(LoanPurpose loanPurpose);
        Task DeleteManyAsync(IEnumerable<string> ids);
    }
}
