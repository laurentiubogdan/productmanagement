﻿using MongoDB.Driver;
using ProductManagement.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Repository
{
    public interface IBaseProductSplitRepository
    {
        IMongoCollection<BaseProductSplits> BaseQuery();
        Task<IEnumerable<BaseProductSplits>> ListAsync();
        Task<BaseProductSplits> CreateAsync(BaseProductSplits baseProductSplits);
        Task<BaseProductSplits> GetAsync(string id);
        Task<BaseProductSplits> UpdateSync(BaseProductSplits baseProdsplits);
        Task DeleteAsync(string id);
    }
}
