﻿using MongoDB.Bson;
using MongoDB.Driver;
using ProductManagement.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductManagement.Repository
{
    public interface IFeatureRepository
    {
        IMongoCollection<Feature> BaseQuery();
        Task<IEnumerable<Feature>> ListAsync();
        Task<Feature> GetAsync(string id);
        Task<Feature> CreateAsync(Feature feature);
        Task<IEnumerable<Feature>> CreateManyAsync(IEnumerable<Feature> features);
        Task<Feature> UpdateAsync(Feature featureIn);
        Task DeleteAsync(Feature featureIn);
        Task DeleteManyAsync(BsonArray featuresIds);
    }
}
